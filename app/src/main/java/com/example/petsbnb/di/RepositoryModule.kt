package com.example.petsbnb.di

import com.example.petsbnb.data.repository.*
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.StorageReference
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Provides
    @Singleton
    fun providePetsRepository(
        database: FirebaseFirestore,
        storageReference: StorageReference
    ): PetsRepository{
        return PetsRepositoryImp(database,storageReference)
    }


    @Provides
    @Singleton
    fun provideSittersRepository(
        database: FirebaseFirestore
    ): SittersRepository {
        return SittersRepositoryImp(database)
    }

    @Provides
    @Singleton
    fun provideReservasRepository(
        database: FirebaseFirestore
    ): ReservaRepository {
        return ReservaRepositoryImp(database)
    }

    @Provides
    @Singleton
    fun provideAuthRepository(
        database: FirebaseFirestore,
        auth: FirebaseAuth
    ): AuthRepository {
        return AuthRepositoryImp(auth,database)
    }

    @Provides
    @Singleton
    fun provideReviewRepository(
        database: FirebaseFirestore,
        storageReference: StorageReference
    ): ReviewRepository{
        return ReviewRepositoryImp(database,storageReference)
    }

    @Provides
    @Singleton
    fun providePlacesRepository(
        database: FirebaseFirestore,
        storageReference: StorageReference
    ): PlaceRepository{
        return PlaceRepositoryImp(database,storageReference)
    }

    @Provides
    @Singleton
    fun provideApp(
    ): App{
        return App()
    }


}