package com.example.petsbnb.view

import android.app.Activity
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.data.model.PetModel
import com.example.petsbnb.databinding.FragmentPetEditBinding
import com.example.petsbnb.util.*
import com.example.petsbnb.viewmodel.PetsViewModel
import com.github.dhaval2404.imagepicker.ImagePicker
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class PetEdit : Fragment() {

    val TAG: String = "PetEdit"
    lateinit var binding: FragmentPetEditBinding
    val viewModel: PetsViewModel by viewModels()
    var isEdit = false
    var objPet: PetModel? = null

    var imageUris: MutableList<Uri> = arrayListOf()
    var imgUri_Storage = ""
    var imgUpload = false

    var needs = false



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPetEditBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        updateUI()

        binding.imgEditPet.setOnClickListener{
         ImagePicker.with(this)
                //.crop()
                .compress(1024)
                .galleryOnly()
                .createIntent { intent ->
                    startForProfileImageResult.launch(intent)
                }
        }


        binding.btnEditAccept.setOnClickListener {

            if(validation() && isOnline(requireContext()) == true){



                viewModel.updatePet(
                    PetModel(
                        id = objPet?.id.toString() ,
                        name = binding.btnEditPetName.text.toString(),
                        age = binding.btnEditPetAge.text.toString(),
                        race = binding.btnEditPetRace.text.toString(),
                        needs = needs,
                        photoURL = imgUri_Storage
                    )
                )
            }
            else if(isOnline(requireContext()) == false){
                toast("Please connect to the internet to update your pet's data")
            }



        }


        binding.btnEditPetCancel.setOnClickListener{
            findNavController().navigate(R.id.action_petEdit_to_petListingfragment)
        }




        viewModel.updatePet.observe(viewLifecycleOwner){ state ->
            when(state){
                is UiState.Loading -> {
                }

                is UiState.Failure -> {
                    toast(state.error)
                }

                is UiState.Success -> {
                    findNavController().navigate(R.id.action_petEdit_to_petListingfragment)
                    // findNavController().navigate(R.id.action_petListingfragment_to_petDetailfragment)
                    toast(state.data)
                }
            }

        }


        binding.btnEditNeeds.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked == true){
                needs = true
            }
        }






    }





    private fun updateUI(){
        val type = arguments?.getString("type",null)
        type?.let{
            when(it){
                "edit" ->{
                    isEdit = true
                    objPet = arguments?.getParcelable("pet")


                    var imgCont  = binding.imgEditPet
                    Picasso.get().load(objPet?.photoURL.toString()).resize(260,260).into(imgCont)

                    binding.btnEditPetName.setText(objPet?.name)
                    binding.btnEditPetAge.setText(objPet?.age)
                    binding.btnEditPetRace.setText(objPet?.race)

                    var needs = false
                    if( objPet?.needs == true ){
                        needs = true
                    }

                    // falta configurar el state del boolean del switch
                    //var xx = binding.btnEditNeeds.isChecked


                }
            }
        }



    }




// funcion para entrar a la carpeta de imagenes y cargar una imagen
    private val startForProfileImageResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->

        val resultCode = result.resultCode
        val data = result.data
        val fileUri = data?.data!!

        if (resultCode == Activity.RESULT_OK) {
            imageUris.add(fileUri)
            var contImg = binding.imgEditPet
            Picasso.get().load(fileUri).resize(260,260).into(contImg)
            imgUpload = true

            //subir imagen a Storage
            uploadImg()

        }
        else if (resultCode == ImagePicker.RESULT_ERROR) {
            toast(ImagePicker.getError(data))
        }
    }




    fun uploadImg(){
        viewModel.onUploadSingleFile(imageUris.first()){ state ->
            when(state){
                is UiState.Loading ->{
                    binding.progressBar2.show()
                    toastShort("Loading Upload Image")
                }
                is UiState.Failure ->{
                    binding.progressBar2.hide()
                    toast(state.error)
                }
                is UiState.Success ->{
                    binding.progressBar2.hide()
                    state
                    imgUri_Storage = state.data.toString()
                    toastShort("Image Upload Succes")

                }
            }

        }
    }













    // validacion del formulario para agregar mascota
    private fun validation(): Boolean{

        var isValid = true

        if(binding.btnEditPetName.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("add your pet's name")
        }
        else if(binding.btnEditPetAge.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("add your pet's Age")
        }
        else if(binding.btnEditPetRace.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("add your pet's race")
        }
        else if(imgUpload == false){
            isValid = false
            toastShort("Upload a image of your pet ")
        }

        return isValid
    }










}