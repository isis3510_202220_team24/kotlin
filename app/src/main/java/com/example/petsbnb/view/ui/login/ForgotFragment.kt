package com.example.petsbnb.view.ui.login

import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentForgotBinding
import com.example.petsbnb.util.UiState
import com.example.petsbnb.view.ui.auth.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForgotFragment: Fragment() {
    val TAG: String = "ForgotFragment"
    lateinit var binding: FragmentForgotBinding
    val viewModel: AuthViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentForgotBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        binding.go.setOnClickListener {
            if (validation()){
                viewModel.forgot(binding.username.text.toString())
            }
        }
        binding.login.setOnClickListener {
            findNavController().navigate(R.id.action_forgotFragment_to_loginFragment)
        }
    }

    private fun observer(){
        viewModel.forgot.observe(viewLifecycleOwner) { state ->
            when(state){
                is UiState.Loading -> {
                    binding.status.text = "Loading"
                }
                is UiState.Failure -> {
                    //binding.forgotPassProgress.hide()
                    binding.status.text = state.error //"Failed to send"
                    //toast(state.error)
                }
                is UiState.Success -> {
                    binding.status.text = state.data
                }
            }
        }
    }

    fun validation(): Boolean {
        var isValid = true

        if (binding.username.text.isNullOrEmpty()){
            isValid = false
        }else{
            if (!isValidEmail(binding.username.text.toString())){
                isValid = false
            }
        }

        return isValid
    }

    private fun isValidEmail(target: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }
}