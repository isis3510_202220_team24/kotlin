package com.example.petsbnb.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentReviewListingfragmentBinding
import com.example.petsbnb.util.*
import com.example.petsbnb.viewmodel.ReviewListingAdapter
import com.example.petsbnb.viewmodel.ReviewViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ReviewListingfragment : Fragment() {

    val TAG: String = "ReviewListingfragment"
    lateinit var binding: FragmentReviewListingfragmentBinding
    val viewModel: ReviewViewModel by viewModels()
    var deletePosition: Int = -1
    val adapter by lazy {
        ReviewListingAdapter(
            onItemClicked = { pos, item ->

            }
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentReviewListingfragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.adapter = adapter

        binding.create.setOnClickListener {

            if( isOnline(requireContext()) == false){
                toast("Please connect to internet")
            }
            else{
                findNavController().navigate(R.id.action_reviewListingfragment_to_reviewDetailfragment)
            }

        }

        lifecycleScope.launch{
            viewModel.getReviews()
            viewModel.reviewModel.observe(viewLifecycleOwner){ state ->
                when(state){
                    is UiState.Loading -> {
                        binding.status.show()
                        binding.status.text = "Loading"
                    }
                    is UiState.Failure -> {
                        binding.status.hide()
                        toast(state.error)
                    }
                    is UiState.Success -> {
                        binding.status.hide()
                        adapter.updateList(state.data.toMutableList())
                    }
                }
            }
        }
    }
}