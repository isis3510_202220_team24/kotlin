package com.example.petsbnb.view.ui.login

import android.net.Uri
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.data.model.UserModel
import com.example.petsbnb.databinding.FragmentUserEditBinding
import com.example.petsbnb.util.UiState
import com.example.petsbnb.util.isOnline
import com.example.petsbnb.util.toast
import com.example.petsbnb.view.ui.auth.AuthViewModel
import com.squareup.picasso.Picasso
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class UserEdit : Fragment() {

        val TAG: String = "UserEdit"
        lateinit var binding: FragmentUserEditBinding
        val viewModel: AuthViewModel by viewModels()
        var isEdit = false
        var objUser: UserModel? = null

        var imageUris: MutableList<Uri> = arrayListOf()
        var imgUri_Storage = ""
        var imgUpload = false



        override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
        ): View {
            binding = FragmentUserEditBinding.inflate(layoutInflater)
            return binding.root
        }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            updateUI()

            //binding.imgEditUser.setOnClickListener{
            //    ImagePicker.with(this)
            //        //.crop()
            //        .compress(1024)
            //        .galleryOnly()
            //        .createIntent { intent ->
            //            startForProfileImageResult.launch(intent)
            //        }
            //}


            binding.btnEditAccept.setOnClickListener {

                if(validation() && isOnline(requireContext()) == true){


                    viewModel.updateUser(
                        binding.name.text.toString(),
                        Integer.parseInt(binding.age.text.toString()),
                        binding.ocupation.text.toString(),
                        imgUri_Storage
                    )
                }
                else if(isOnline(requireContext()) == false){
                    toast("Please connect to the internet to update your data")
                }
                findNavController().navigate(R.id.action_userEdit_to_ProfileFragment)
            }


            binding.btnEditPetCancel.setOnClickListener{
                findNavController().navigate(R.id.action_userEdit_to_ProfileFragment)
            }


            viewModel.updateUser.observe(viewLifecycleOwner){ state ->
                when(state){
                    is UiState.Loading -> {
                    }

                    is UiState.Failure -> {
                        toast(state.error)
                    }

                    is UiState.Success -> {
                        findNavController().navigate(R.id.action_petEdit_to_petListingfragment)
                        // findNavController().navigate(R.id.action_petListingfragment_to_petDetailfragment)
                        toast(state.data)
                    }
                }

            }
        }

        private fun updateUI(){
            val type = arguments?.getString("type",null)
            type?.let{

                when(it){
                    "edit" ->{
                        isEdit = true
                        objUser = arguments?.getParcelable("user")

                        var imgCont  = binding.imgEditUser
                        Picasso.get().load(objUser?.photoURL.toString()).resize(260,260).into(imgCont)
                    }
                }
            }
        }


        // funcion para entrar a la carpeta de imagenes y cargar una imagen
        //private val startForProfileImageResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) { result: ActivityResult ->
        //    val resultCode = result.resultCode
        //    val data = result.data
        //    val fileUri = data?.data!!
        //    if (resultCode == Activity.RESULT_OK) {
        //        imageUris.add(fileUri)
        //        var contImg = binding.imgEditUser
        //        Picasso.get().load(fileUri).resize(260,260).into(contImg)
        //        imgUpload = true
        //        //subir imagen a Storage
        //        uploadImg()
        //    }
        //    else if (resultCode == ImagePicker.RESULT_ERROR) {
        //        toast(ImagePicker.getError(data))
        //    }
        //}




        //fun uploadImg(){
        //    viewModel.onUploadSingleFile(imageUris.first()){ state ->
        //        when(state){
        //            is UiState.Loading ->{
        //                toastShort("Loading Upload Image")
        //            }
        //            is UiState.Failure ->{
        //                toast(state.error)
        //            }
        //            is UiState.Success ->{
        //                //state
        //                imgUri_Storage = state.data.toString()
        //                toast("Image Upload Succes")
        //            }
        //        }
        //    }
        //}



        fun validation(): Boolean {
            var isValid = true

            if (binding.name.text.isNullOrEmpty()){
                isValid = false
                toast("Enter name")
            }

            if (binding.age.text.isNullOrEmpty()){
                isValid = false

                toast("Enter age")
            }else{
                if (!binding.age.text.toString().isDigitsOnly()){
                    isValid = false

                    toast("Invalid age")
                }else
                {
                    if (Integer.parseInt(binding.age.text.toString()) < 13 || Integer.parseInt(binding.age.text.toString()) > 100){
                        isValid = false
                        toast("Age must be between 13 and 100")
                    }
                }
            }

            if (binding.ocupation.text.isNullOrEmpty()){
                isValid = false
                toast("Enter occupation")
            }


            if (!isOnline(requireContext())){
                isValid = false
                toast("Connect to internet to login")

            }
            return isValid
        }

        private fun isValidEmail(target: CharSequence?): Boolean {
            return if (TextUtils.isEmpty(target)) {
                false
            } else {
                Patterns.EMAIL_ADDRESS.matcher(target).matches()
            }
        }






    }
