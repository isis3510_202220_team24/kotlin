package com.example.petsbnb.view

import android.os.Bundle
import android.os.CountDownTimer
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentPetListingfragmentBinding
import com.example.petsbnb.util.*
import com.example.petsbnb.viewmodel.PetListingAdapter
import com.example.petsbnb.viewmodel.PetsViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch

@AndroidEntryPoint
class PetListingfragment : Fragment() {

    val TAG: String = "PetListingfragment"
    lateinit var binding: FragmentPetListingfragmentBinding
    val viewModel: PetsViewModel by viewModels()
    var deletePosition: Int = -1
    val adapter by lazy {
        PetListingAdapter(
            onItemClicked = { pos, item ->

            },
            onEditClicked = { pos, item ->


                if( isOnline(requireContext()) == true ){

                    findNavController().navigate(R.id.action_petListingfragment_to_petEdit,Bundle().apply {
                        putString("type","edit")
                        putParcelable("pet",item)
                    })

                }
                else{
                    toast("Please connect to the internet to edit this pet")
                }

            },
            onDeleteClicked = { pos, item ->

                if( isOnline(requireContext()) == true ){
                    deletePosition = pos
                    viewModel.deletePet(item)
                   // item?.let { viewModel.deletePet(item) }


                }
                else{
                    toast("Please connect to the internet to delete this pet")
                }

            }

        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentPetListingfragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.adapter = adapter

        //navegacion
        binding.btnCreate.setOnClickListener {

            if( isOnline(requireContext()) == false){
                toast("You need internet connection to add a new pet")
            }
            else{
                findNavController().navigate(R.id.action_petListingfragment_to_petDetailfragment)
            }

        }




        lifecycleScope.launch{
            viewModel.getPets()
            viewModel.petModel.observe(viewLifecycleOwner){ state ->
                when(state){
                    is UiState.Loading -> {
                        binding.progressBarPets.show()
                    }
                    is UiState.Failure -> {
                        binding.progressBarPets.hide()
                        toast(state.error)
                    }
                    is UiState.Success -> {
                        binding.progressBarPets.hide()
                        adapter.updateList(state.data.toMutableList())
                    }
                }
            }
        }




        viewModel.deletePet.observe(viewLifecycleOwner){ state ->
            when(state){
                is UiState.Loading -> {
                    binding.progressBarPets.show()
                }
                is UiState.Failure -> {
                    binding.progressBarPets.hide()
                    toast(state.error)
                }
                is UiState.Success -> {
                    binding.progressBarPets.hide()
                    adapter.removeItem(deletePosition)
                    toast(state.data)
                }
            }

        }







    }






}