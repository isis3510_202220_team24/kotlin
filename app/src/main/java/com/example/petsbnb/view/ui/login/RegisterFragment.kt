package com.example.petsbnb.view.ui.login

import android.content.Context
import android.os.Bundle
import android.text.TextUtils
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.data.model.UserModel
import com.example.petsbnb.databinding.FragmentRegisterBinding
import com.example.petsbnb.util.UiState
import com.example.petsbnb.util.isOnline
import com.example.petsbnb.view.ui.auth.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class RegisterFragment : Fragment() {

    val TAG: String = "RegisterFragment"
    lateinit var binding: FragmentRegisterBinding
    val viewModel: AuthViewModel by viewModels()
    //val userViewModel: UserViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentRegisterBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observer()
        binding.register.setOnClickListener {
            if(isOnline(requireContext())){
                if (validation()){
                    viewModel.register(
                        email = binding.username.text.toString(),
                        password = binding.password.text.toString(),
                        user = getUserObj()
                    )



                }
            }
        }

        binding.login.setOnClickListener {
            findNavController().navigate(R.id.action_registerFragment_to_loginFragment)
        }
    }

    fun observer() {
        viewModel.register.observe(viewLifecycleOwner) { state ->
            when(state){
                is UiState.Loading -> {
                    binding.status.text = "Loading"
                }
                is UiState.Failure -> {
                    binding.status.text = state.error // "Failure in register"
                }
                is UiState.Success -> {
                    val sharedPref = this.getActivity()?.getSharedPreferences(
                        "username", Context.MODE_PRIVATE)
                    if (sharedPref != null) {
                        sharedPref.edit().putString("username", binding.username.text.toString()).apply()
                    }
                    binding.status.setText("Registering to Petsbnb")
                    viewModel.register(
                        binding.name.text.toString(),
                        binding.password.text.toString(),
                        UserModel(
                            age = Integer.parseInt(binding.age.text.toString()),
                            name = binding.name.text.toString(),
                            username = binding.username.text.toString(),
                            ocupation = binding.ocupation.text.toString()
                        )
                    )
                    findNavController().navigate(R.id.action_registerFragment_to_petListingFragment)

                }
            }
        }
    }

    fun getUserObj(): UserModel {
        return UserModel(
            id = "",
            name = binding.name.text.toString(),
            age = Integer.parseInt(binding.age.text.toString()),
            ocupation = binding.ocupation.text.toString(),
            username = binding.username.text.toString(),
        )
    }

    fun validation(): Boolean {
        var isValid = true

        if (binding.name.text.isNullOrEmpty()){
            isValid = false
            binding.status.text = "Enter name"
        }

        if (binding.age.text.isNullOrEmpty()){
            isValid = false
            binding.status.text = "Enter aage"
        }else{
            if (!binding.age.text.toString().isDigitsOnly()){
                isValid = false
                binding.status.text = "Invalid age"
            }else
            {
                if (Integer.parseInt(binding.age.text.toString()) < 13 || Integer.parseInt(binding.age.text.toString()) > 100){
                    isValid = false
                    binding.status.text = "Age must be between 13 and 100"
                }
            }
        }

        if (binding.ocupation.text.isNullOrEmpty()){
            isValid = false
            binding.status.text = "Enter occupation"
        }

        if (binding.username.text.isNullOrEmpty()){
            isValid = false
            binding.status.text = "Enter email"
        }else{
            if (!isValidEmail(binding.username.text)){
                isValid = false
                binding.status.text = "Invalid email"
            }
        }
        if (binding.password.text.isNullOrEmpty()){
            isValid = false
            binding.status.text = "Enter a password"
        }else{
            if (binding.password.text.toString().length < 5){
                isValid = false
                binding.status.text = "Invalid password"
            }
        }
        if (!isOnline(requireContext())){
            isValid = false
            binding.status.text = "Connect to internet to login"

        }
        return isValid
    }

    private fun isValidEmail(target: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

}