package com.example.petsbnb.view.ui

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentPlacesListingBinding
import com.example.petsbnb.util.UiState
import com.example.petsbnb.util.hide
import com.example.petsbnb.util.show
import com.example.petsbnb.util.toast
import com.example.petsbnb.viewmodel.PlacesListingAdapter
import com.example.petsbnb.viewmodel.PlacesViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class PlacesListing : Fragment() {
    val TAG: String = "PlacesListingFragment"
    lateinit var binding: FragmentPlacesListingBinding
    val viewModel: PlacesViewModel by viewModels()
    val adapter by lazy{
        PlacesListingAdapter(
            onItemClicked = { pos, item ->}
        )
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentPlacesListingBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.adapter = adapter

        lifecycleScope.launch{

            viewModel.getPLaces()
            viewModel.PlaceModel.observe(viewLifecycleOwner){ state ->
                when(state){
                    is UiState.Loading ->{
                        binding.progresBarPlace.show()
                    }
                    is UiState.Failure -> {
                        binding.progresBarPlace.hide()
                        toast(state.error)
                    }
                    is UiState.Success -> {
                        binding.progresBarPlace.hide()
                        adapter.updateList(state.data.toMutableList())

                    }
                }
            }

        }

    }

}