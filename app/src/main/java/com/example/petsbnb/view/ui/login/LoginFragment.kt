package com.example.petsbnb.view.ui.login

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.util.Patterns
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentLoginBinding
import com.example.petsbnb.util.UiState
import com.example.petsbnb.view.ui.auth.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class LoginFragment : Fragment() {

    val TAG: String = "LoginFragment"
    lateinit var binding: FragmentLoginBinding
    val viewModel: AuthViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentLoginBinding.inflate(layoutInflater)
        return binding.root
    }




    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)
        observer()
        binding.login.setOnClickListener {


            if(isOnline(requireContext())) {
                if (validation()) {
                    viewModel.login(
                        email = binding.username.text.toString(),
                        password = binding.password.text.toString()
                    )
                }
            }

        }

        binding.register.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_registerFragment)
        }

        binding.forgot.setOnClickListener {
            findNavController().navigate(R.id.action_loginFragment_to_forgotFragment)
        }
    }

    fun observer() {
        viewModel.login.observe(viewLifecycleOwner) { state ->
            when(state){
                is UiState.Loading -> {
                    binding.status.text = "Loading"
                }
                is UiState.Failure -> {
                    binding.status.text = state.error
                }
                is UiState.Success -> {
                    val sharedPref = this.getActivity()?.getSharedPreferences(
                        "username", Context.MODE_PRIVATE)
                    if (sharedPref != null) {
                        sharedPref.edit().putString("username", binding.username.text.toString()).apply()
                    }
                    binding.status.text = state.data
                    findNavController().navigate(R.id.action_loginFragment_to_petListingFragment)

                }
            }
        }
    }


    fun validation(): Boolean {
        var isValid = true

        if (binding.username.text.isNullOrEmpty()){
            isValid = false
            binding.status.text = "Enter email"
        }else{
            if (!isValidEmail(binding.username.text)){
                isValid = false
                binding.status.text = "Invalid email"
            }
        }
        if (binding.password.text.isNullOrEmpty()){
            isValid = false
            binding.status.text = "Enter a password"
        }

        if (!isOnline(requireContext())){
            isValid = false
            binding.status.text = "Connect to internet to login"

        }
        return isValid
    }

    private fun isValidEmail(target: CharSequence?): Boolean {
        return if (TextUtils.isEmpty(target)) {
            false
        } else {
            Patterns.EMAIL_ADDRESS.matcher(target).matches()
        }
    }

    private fun isOnline(context: Context): Boolean {
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (connectivityManager != null) {
            val capabilities =
                connectivityManager.getNetworkCapabilities(connectivityManager.activeNetwork)
            if (capabilities != null) {
                if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_CELLULAR")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_WIFI")
                    return true
                } else if (capabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET)) {
                    Log.i("Internet", "NetworkCapabilities.TRANSPORT_ETHERNET")
                    return true
                }
            }
        }
        return false
    }

}