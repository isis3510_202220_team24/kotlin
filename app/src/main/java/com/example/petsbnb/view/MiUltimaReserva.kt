package com.example.petsbnb.view

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentMiUltimaReservaBinding



class MiUltimaReserva : Fragment() {

    lateinit var binding: FragmentMiUltimaReservaBinding


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentMiUltimaReservaBinding.inflate(layoutInflater)
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val sharedPreference =
            activity!!.getSharedPreferences("UltimaReserva", Context.MODE_PRIVATE)

        binding.txtInitialDateLocal.setText(sharedPreference.getString("dateIni", "No date init"))
        binding.txtEndDateLocal.setText(sharedPreference.getString("dateEnd", "No date end"))
        binding.txtCostLocal.setText(sharedPreference.getString("cost", "No cost value"))



        binding.btnBackLocal.setOnClickListener{
            findNavController().navigate(R.id.action_miUltimaReserva_to_home2)
        }




    }





}