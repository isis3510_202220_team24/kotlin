package com.example.petsbnb.view

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Bitmap.CompressFormat
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.data.model.PetModel
import com.example.petsbnb.databinding.FragmentPetDetailfragmentBinding
import com.example.petsbnb.util.*
import com.example.petsbnb.viewmodel.PetsViewModel
import dagger.hilt.android.AndroidEntryPoint
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream


@AndroidEntryPoint
class PetDetailfragment : Fragment() {

    val TAG: String = "PetDetailfragment"
    lateinit var binding: FragmentPetDetailfragmentBinding
    val viewModel: PetsViewModel by viewModels()


    //para cargar la imagen en fireStorage
    var imageUris: MutableList<Uri> = arrayListOf()
    var imgUri_Storage = ""
    var imgUpload = false
    //var imageCamara = binding.imgCam
    var objPet: PetModel? = null

    var appContext = null

    var needs = false



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

       // appContext = context!!.applicationContext

        binding = FragmentPetDetailfragmentBinding.inflate(layoutInflater)
        return binding.root
    }



    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        binding.btnCreatePet.setOnClickListener {


            if( isOnline( requireContext() ) == false ){
                toast("Not Internet conection. Please conect to internet for add your pet")

            }
            else if(validation() && isOnline( requireContext() ) == true  ){



                viewModel.addPet(
                    PetModel(
                            id= "",
                            age = binding.txtInpAge.text.toString(),
                            name = binding.txtInpName.text.toString(),
                            race = binding.txtInpRace.text.toString(),
                            needs = needs,
                            photoURL= "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Dog-512.png",
                            //photoURL = imgUri_Storage
                    )
                )

                viewModel.addPet.observe(viewLifecycleOwner){ state ->
                    when(state){
                        is UiState.Loading -> {
                            binding.btnProgressAr.show()
                            binding.btnCreatePet.text = ""
                        }

                        is UiState.Failure -> {
                            binding.btnProgressAr.hide()
                            binding.btnCreatePet.text = "Create"
                            toast(state.error)
                        }

                        is UiState.Success -> {
                            binding.btnProgressAr.hide()
                            binding.btnCreatePet.text = "Create"

                            findNavController().navigate(R.id.action_petDetailfragment_to_petListingfragment)
                            // findNavController().navigate(R.id.action_petListingfragment_to_petDetailfragment)

                            toast(state.data)
                        }
                    }
                }


        }
    }



        binding.btnNeeds.setOnCheckedChangeListener { buttonView, isChecked ->
            if(isChecked == true){
                needs = true
            }
        }









//capturar imagen de la camara
     binding.btnCamara.setOnClickListener {
         startForResult.launch(  Intent(MediaStore.ACTION_IMAGE_CAPTURE) )
        //findNavController().navigate(R.id.action_petDetailfragment_to_cameraXApp)
     }



    }







    // funcion para asetear la imagen tomada por la camara
    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
            result: ActivityResult ->
        if(result.resultCode == Activity.RESULT_OK){


            var intent = result.data
            var imageBitmap = intent?.extras?.get("data") as Bitmap
            var imageView = binding.imgCam
            imageView.setImageBitmap(imageBitmap)

            ///var f = saveMediaToStorage(imageBitmap)
            //var bool2 =   ImageUtils.save2Album(imageBitmap, Bitmap.CompressFormat.JPEG, 50)

          //  var uriss = Uri.fromFile()

            //var uriss = Uri.fromFile(bool2)
            //imageUris.add(uriss)







  //          viewModel.onUploadSingleFile(imageUris.first()){ state ->
  //                  when (state){
  //                      is UiState.Loading ->{
  //                          binding.progresBarCam.show()
  //                          toastShort("Loading Upload Image")
   //                     }
   //                     is UiState.Failure -> {
   //                         binding.progresBarCam.hide()
   //                         toast(state.error)
   //                     }
    //                    is UiState.Success -> {
    //                        binding.progresBarCam.hide()
    //                        state
    //                        imgUri_Storage = state.data.toString()
    //                        imgUpload = true
    //                        toastShort("Image Upload Succes")
    //                    }
    //                }
    //            }

        }
    }



// validacion del formulario para agregar mascota
    private fun validation(): Boolean{

        var isValid = true

        if(binding.txtInpName.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("add your pet's name")
        }
        else if(binding.txtInpAge.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("add your pet's Age")
        }
        else if(binding.txtInpRace.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("add your pet's race")
        }
        //else if(imgUpload == false){
        //    isValid = false
        //    toastShort("take picture of your pet ")
        ///}

        return isValid
    }







    fun saveMediaToStorage(bitmap: Bitmap) {
        //Generating a file name
        val filename = "${System.currentTimeMillis()}.jpg"

        //Output stream
        var fos: OutputStream? = null

        //For devices running android >= Q
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            //getting the contentResolver
            context?.contentResolver?.also { resolver ->

                //Content resolver will process the contentvalues
                val contentValues = ContentValues().apply {

                    //putting file information in content values
                    put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                }

                //Inserting the contentValues to contentResolver and getting the Uri
                val imageUri: Uri? =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

                //Opening an outputstream with the Uri that we got
                fos = imageUri?.let { resolver.openOutputStream(it) }
            }
        } else {
            //These for devices running on android < Q
            //So I don't think an explanation is needed here
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val image = File(imagesDir, filename)
            fos = FileOutputStream(image)
        }

        fos?.use {
            //Finally writing the bitmap to the output stream that we opened
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
           // context?.toast("Saved to Photos")
        }
    }
















}