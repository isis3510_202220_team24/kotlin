package com.example.petsbnb.view

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CalendarView.OnDateChangeListener
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.example.petsbnb.data.model.ReservaModel
import com.example.petsbnb.databinding.FragmentReservaCreateBinding
import com.example.petsbnb.util.*
import com.example.petsbnb.viewmodel.ReservasViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.text.SimpleDateFormat
import java.time.LocalDateTime
import java.time.format.DateTimeFormatter
import java.util.*
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import java.time.temporal.ChronoUnit


@AndroidEntryPoint
class ReservaCreate : Fragment() {


    lateinit var binding: FragmentReservaCreateBinding
    val viewModel: ReservasViewModel by viewModels()

    var fechaInicial = ""
    var fechaFinal = ""
    var fechaDia = ""
    var boolDias = false



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentReservaCreateBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnCalendarEnd.hide()
        binding.txtEndTitleCalendar.hide()

        binding.btnCalendarInit.hide()
        binding.txtStartDayCalendar.hide()

        binding.btnCalendarOnly.show()


        // time formato 24
        binding.btnTimeInit.setIs24HourView(true)
        binding.btnTimeEnd.setIs24HourView(true)

        // set format calendar
        binding.btnCalendarInit.setMinDate(System.currentTimeMillis() - 1000) //no permite elegir fechas anteriores a la actual
        binding.btnCalendarEnd.setMinDate(System.currentTimeMillis() + 86400000  )
        binding.btnCalendarOnly.setMinDate(System.currentTimeMillis() - 1000) //no permite elegir fechas anteriores a la actual


        //set fechas iniciales
        val sdf = SimpleDateFormat("dd/MM/yyyy")
        val selectedDates = sdf.format( binding.btnCalendarInit.date)
        fechaInicial = selectedDates
        fechaFinal = selectedDates
        fechaDia = selectedDates

        //Captura la selecciond de una nueva fecha
        binding.btnCalendarInit.setOnDateChangeListener(OnDateChangeListener { view, year, month, dayOfMonth ->
            val selectedDates = sdf.format(Date(year-1900, month, dayOfMonth))
            fechaInicial = selectedDates
            drawCost()
        })
        binding.btnCalendarEnd.setOnDateChangeListener(OnDateChangeListener { view, year, month, dayOfMonth ->
            val selectedDates = sdf.format(Date(year-1900, month, dayOfMonth))
            fechaFinal = selectedDates
            drawCost()
        })
        binding.btnCalendarOnly.setOnDateChangeListener(OnDateChangeListener { view, year, month, dayOfMonth ->
            val selectedDates = sdf.format(Date(year-1900, month, dayOfMonth))
            fechaDia = selectedDates
            drawCost()
        })



        binding.btnBoolMultDays.setOnCheckedChangeListener { buttonView, isChecked ->
            boolDias = isChecked

            if(isChecked == true){
                binding.btnCalendarEnd.show()
                binding.txtEndTitleCalendar.show()

                binding.txtStartDayCalendar.show()
                binding.btnCalendarInit.show()

                binding.btnCalendarOnly.hide()
            }
            else{
                binding.btnCalendarEnd.hide()
                binding.txtEndTitleCalendar.hide()

                binding.txtStartDayCalendar.hide()
                binding.btnCalendarInit.hide()

                binding.btnCalendarOnly.show()
            }
            drawCost()
        }


        binding.btnTimeInit.setOnTimeChangedListener { view, hourOfDay, minute ->
            drawCost()
        }
        binding.btnTimeEnd.setOnTimeChangedListener { view, hourOfDay, minute ->
            drawCost()
        }






        binding.btnReserveAccept.setOnClickListener{


            if( isOnline(requireContext()) == true ){

                // varios dias
                if( boolDias == true ){

                    var numDias = 0

                    val sptD1 = fechaInicial.split('/')
                    val sptD2 = fechaFinal.split('/')


                    val date1 = LocalDateTime.parse(sptD1[2]+"-"+sptD1[1]+"-"+sptD1[0]+"T20:00:00.0400")
                    val date2 = LocalDateTime.parse(sptD2[2]+"-"+sptD2[1]+"-"+sptD2[0]+"T20:00:00.0400")

                    numDias = ChronoUnit.DAYS.between(date1, date2).toInt()

                    if(numDias <= 0){
                        toastShort("the days are wrong")
                    }
                    else{
                        // hora inicial
                        var ini_h = binding.btnTimeInit.currentHour
                        var ini_m = binding.btnTimeInit.currentMinute
                        // hora Final
                        var end_h = binding.btnTimeEnd.currentHour
                        var end_m = binding.btnTimeEnd.currentMinute


                        var costo = numDias*10
                        var commentario = binding.textBoxComment.text.toString()

                        var fecha_init_F = fechaInicial.toString()+" "+ini_h+":"+ini_m
                        var fecha_end_F = fechaFinal.toString()+" "+end_h+":"+end_m


                        viewModel.addReserva(
                            ReservaModel(
                                comment = commentario,
                                cost=costo,
                                finishDate=fecha_end_F,
                                initialDate=fecha_init_F,
                                multipleDays=true
                            )
                        )
                        viewModel.addReserva.observe(viewLifecycleOwner){ state ->
                            when(state){
                                is UiState.Loading -> {
                                    binding.btnProgressReserva.show()
                                    binding.btnReserveAccept.text = ""
                                }

                                is UiState.Failure -> {
                                    binding.btnProgressReserva.hide()
                                    binding.btnReserveAccept.text = "Create"
                                    toast(state.error)
                                }

                                is UiState.Success -> {
                                    binding.btnProgressReserva.hide()
                                    binding.btnReserveAccept.text = "Create"

                                    miUltimaReserva(fecha_init_F,fecha_end_F,costo)

                                    //findNavController().navigate(R.id.action_reservaCreate_to_reservasListing)
                                    findNavController().navigate(R.id.action_reservaCreate_to_home2)
                                    toast(state.data)
                                }
                            }
                        }

                    }

                } //solo horas
                else{
                    val current = LocalDateTime.now()
                    //println("Current Date and Time is: $current")

                    // hora inicial
                    var ini_h = binding.btnTimeInit.currentHour
                    var ini_m = binding.btnTimeInit.currentMinute
                    // hora Final
                    var end_h = binding.btnTimeEnd.currentHour
                    var end_m = binding.btnTimeEnd.currentMinute


                    var boolHoraIni  =validHoraActual(ini_h,ini_m)
                    var boolHoraEnd  =validHoraActual(end_h,end_m)



                    if(boolHoraIni == true and boolHoraEnd == true and (ini_h <= end_h) ){

                        var costo = end_h - ini_h
                        if(costo <= 0){
                            costo = 1
                        }
                        costo = costo*2

                        /// Paramatros para crear la reserva
                        // text box
                        var commentario = binding.textBoxComment.text.toString()

                        var fecha_init_F = fechaDia.toString()+" "+ini_h+":"+ini_m
                        var fecha_end_F = fechaDia.toString()+" "+end_h+":"+end_m

                        viewModel.addReserva(
                            ReservaModel(
                                comment = commentario,
                                cost=costo,
                                finishDate=fecha_init_F,
                                initialDate=fecha_end_F,
                                multipleDays=false
                            )
                        )
                        viewModel.addReserva.observe(viewLifecycleOwner){ state ->
                            when(state){
                                is UiState.Loading -> {
                                    binding.btnProgressReserva.show()
                                    binding.btnReserveAccept.text = ""
                                }

                                is UiState.Failure -> {
                                    binding.btnProgressReserva.hide()
                                    binding.btnReserveAccept.text = "Create"
                                    toast(state.error)
                                }

                                is UiState.Success -> {
                                    binding.btnProgressReserva.hide()
                                    binding.btnReserveAccept.text = "Create"

                                    miUltimaReserva(fecha_init_F,fecha_end_F,costo)

                                    //findNavController().navigate(R.id.action_reservaCreate_to_reservasListing)}
                                    findNavController().navigate(R.id.action_reservaCreate_to_home2)
                                        toast(state.data)
                                }
                            }
                        }
                    }
                    else{
                        toast("inconsistency in start and end time")
                    }
                }


            }else{
                toast("Please connect to the internet to create reserve")
            }



        }



        binding.btnReserveCancel.setOnClickListener{
           findNavController().navigate(R.id.action_reservaCreate_to_sitterlistingfragment)

        }









    }








    fun validHoraActual(pHora: Int,pMinute: Int): Boolean{

        val current = LocalDateTime.now()
        var valided = false

       // println("Current Date and Time is: $current")

        val formatterH = DateTimeFormatter.ofPattern("HH")
        val h_actual = current.format(formatterH)

        val formatterM = DateTimeFormatter.ofPattern("mm")
        val m_actual = current.format(formatterM)

        if( pHora <  h_actual.toInt() ){
            toastShort("the time is less than the current one ")
            return valided
        }
        else if( pHora == h_actual.toInt() ){

            if(pMinute < m_actual.toInt() ){
                toastShort("the Minute is less than the current one ")
                return valided
            }
            else{
                valided = true
                return valided
            }
        }
        else{
            valided = true
            return valided
        }
    }


    fun drawCost(){
        if( boolDias == true ){

            var numDias = 0

            val sptD1 = fechaInicial.split('/')
            val sptD2 = fechaFinal.split('/')


            val date1 = LocalDateTime.parse(sptD1[2]+"-"+sptD1[1]+"-"+sptD1[0]+"T20:00:00.0400")
            val date2 = LocalDateTime.parse(sptD2[2]+"-"+sptD2[1]+"-"+sptD2[0]+"T20:00:00.0400")

            numDias = ChronoUnit.DAYS.between(date1, date2).toInt()

            var costo = numDias*10

            if( costo <= 0 ){
                costo = 0
            }

            binding.textTotalCost.setText((costo).toString())

        }else {


            val current = LocalDateTime.now()
            println("Current Date and Time is: $current")

            // hora inicial
            var ini_h = binding.btnTimeInit.currentHour
            var ini_m = binding.btnTimeInit.currentMinute
            // hora Final
            var end_h = binding.btnTimeEnd.currentHour
            var end_m = binding.btnTimeEnd.currentMinute


            var boolHoraIni = validHoraActualSinToast(ini_h, ini_m)
            var boolHoraEnd = validHoraActualSinToast(end_h, end_m)



            if (boolHoraIni == true and boolHoraEnd == true and (ini_h <= end_h)) {

                var costo = end_h - ini_h
                if (costo <= 0) {
                    costo = 1
                }

                var costof = costo * 2

                binding.textTotalCost.setText((costof).toString())
            }
        }
    }




    fun validHoraActualSinToast(pHora: Int,pMinute: Int): Boolean{

        val current = LocalDateTime.now()
        var valided = false

        // println("Current Date and Time is: $current")

        val formatterH = DateTimeFormatter.ofPattern("HH")
        val h_actual = current.format(formatterH)

        val formatterM = DateTimeFormatter.ofPattern("mm")
        val m_actual = current.format(formatterM)

        if( pHora <  h_actual.toInt() ){
            return valided
        }
        else if( pHora == h_actual.toInt() ){

            if(pMinute < m_actual.toInt() ){
                return valided
            }
            else{
                valided = true
                return valided
            }
        }
        else{
            valided = true
            return valided
        }
    }


    fun miUltimaReserva(pDateIni:String,pDateEnd:String,pCost:Int){
        val sharedPreference =  activity!!.getSharedPreferences("UltimaReserva", Context.MODE_PRIVATE)
        var editor = sharedPreference.edit()
        editor.putString("dateIni",pDateIni)
        editor.putString("dateEnd",pDateEnd)
        editor.putString("cost",pCost.toString())
        editor.commit()
    }







}