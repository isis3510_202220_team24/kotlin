package com.example.petsbnb.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import com.example.petsbnb.databinding.FragmentReservasListingBinding
import com.example.petsbnb.util.*
import com.example.petsbnb.viewmodel.ReservasViewModel
import com.example.petsbnb.viewmodel.ReservaListingAdapter
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch


@AndroidEntryPoint
class ReservasListing : Fragment() {

    lateinit var binding: FragmentReservasListingBinding
    val viewModel: ReservasViewModel by viewModels()
    val adapter by lazy {
        ReservaListingAdapter(
            onItemClicked = { pos, item ->}
        )
    }




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentReservasListingBinding.inflate(layoutInflater)
        return binding.root
    }





    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.adapter = adapter


        lifecycleScope.launch {

            viewModel.getReservas()
            viewModel.reservaModel.observe(viewLifecycleOwner) { state ->
                when (state) {
                    is UiState.Loading -> {
                        binding.progressBarReservas.show()
                    }
                    is UiState.Failure -> {
                        binding.progressBarReservas.hide()
                        toast(state.error)
                    }
                    is UiState.Success -> {
                        binding.progressBarReservas.hide()
                        adapter.updateList(state.data.toMutableList())

                        if( isOnline(requireContext()) == false ){
                            toast("please connect to the internet in case you have any pending reservation")
                            }

                        }

                }
            }

        }


    }






}