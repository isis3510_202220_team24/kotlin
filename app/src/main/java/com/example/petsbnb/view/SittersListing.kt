package com.example.petsbnb.view

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.viewModels
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.databinding.FragmentSittersListingBinding
import com.example.petsbnb.viewmodel.SitterListingAdapter
import com.example.petsbnb.viewmodel.SittersViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.launch
import com.example.petsbnb.R
import com.example.petsbnb.util.*

@AndroidEntryPoint
class SittersListing : Fragment() {

    val TAG: String = "SitterListingfragment"
    lateinit var binding: FragmentSittersListingBinding
    val viewModel: SittersViewModel by viewModels()
    val adapter by lazy {
        SitterListingAdapter(
            onItemClicked = { pos, item ->},
            onReservaClicked = { pos, item ->

                if( isOnline(requireContext()) == true ){
                    findNavController().navigate(R.id.action_sitterlistingfragment_to_reservaCreate)
                }
                else{
                    toast("Please connect to the internet to create reserve with this Sitter")
                }



            }
        )
    }




    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentSittersListingBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.recyclerView.adapter = adapter


        lifecycleScope.launch {

            viewModel.getSitters()
            viewModel.SitterModel.observe(viewLifecycleOwner) { state ->
                when (state) {
                    is UiState.Loading -> {
                        binding.progressBarSitters.show()
                    }
                    is UiState.Failure -> {
                        binding.progressBarSitters.hide()
                        toast(state.error)
                    }
                    is UiState.Success -> {
                        binding.progressBarSitters.hide()
                        adapter.updateList(state.data.toMutableList())
                    }
                }
            }

        }


    }



}