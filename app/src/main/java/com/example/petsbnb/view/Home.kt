package com.example.petsbnb.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentHomeBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class Home : Fragment() {

    lateinit var binding: FragmentHomeBinding





    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root




    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnGoPets.setOnClickListener{
            findNavController().navigate(R.id.action_home2_to_petListingfragment)
        }

        binding.btnGoSitters.setOnClickListener{
            findNavController().navigate(R.id.action_home2_to_sitterlistingfragment)
        }

        binding.signout.setOnClickListener{
            findNavController().navigate(R.id.action_home2_to_loginfragment)
        }

        binding.btnMisreservas.setOnClickListener{
            findNavController().navigate(R.id.action_home2_to_reservasListing)
        }
        binding.btnMylastReserveHome.setOnClickListener{
            findNavController().navigate(R.id.action_home2_to_miUltimaReserva)

        }

        binding.btnPlacesList.setOnClickListener{
            findNavController().navigate(R.id.action_home2_to_placesListing)

        }


    }





}