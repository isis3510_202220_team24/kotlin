package com.example.petsbnb.view.ui.auth

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.petsbnb.data.model.UserModel
import com.example.petsbnb.data.repository.AuthRepository
import com.example.petsbnb.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject


@HiltViewModel
class AuthViewModel @Inject constructor(
    val repository: AuthRepository
): ViewModel() {

    private val _register = MutableLiveData<UiState<String>>()
    val register: LiveData<UiState<String>>
        get() = _register

    private val _login = MutableLiveData<UiState<String>>()
    val login: LiveData<UiState<String>>
        get() = _login

    private val _updateUser = MutableLiveData<UiState<String>>()
    val updateUser: LiveData<UiState<String>>
        get() = _updateUser

    private val _getName = MutableLiveData<UiState<String>>()
    val getName: LiveData<UiState<String>>
        get() = _getName

    private val _getOcupation = MutableLiveData<UiState<String>>()
    val getOcupation: LiveData<UiState<String>>
        get() = _getOcupation

    private val _getAge = MutableLiveData<UiState<String>>()
    val getAge: LiveData<UiState<String>>
        get() = _getAge

    private val _forgot = MutableLiveData<UiState<String>>()
    val forgot: LiveData<UiState<String>>
        get() = _forgot

    fun register(
        email: String,
        password: String,
        user: UserModel
    ) {
        _register.value = UiState.Loading
        repository.register(
            email = email,
            password = password,
            user = user
        ) { _register.value = it }
    }

    fun login(
        email: String,
        password: String
    ) {
        _login.value = UiState.Loading
        repository.login(
            email = email,
            password = password
        ) { _login.value = it }
    }

    fun forgot(email: String) {
        _forgot.value = UiState.Loading
        repository.forgot(email){
            _forgot.value = it
        }
    }

    fun getCurrentUser () : String{
        return repository.getCurrentUserMail()
    }

    fun getName () : String{
        return repository.getName()
    }

    fun getOcupation () : String{
        return repository.getOcupation()
    }

    fun getAge () : String{
        return repository.getAge()
    }


    fun updateUser(name: String, age: Int, ocupation: String, imguriStorage: String) {
        // TODO
    }


    //fun onUploadSingleFile(fileUris: Uri, onResult: (UiState<Uri>) -> Unit){
    //    onResult.invoke(UiState.Loading)
    //    viewModelScope.launch {
    //        repository.uploadSingleFile(fileUris,onResult)
    //    }
    //}
}