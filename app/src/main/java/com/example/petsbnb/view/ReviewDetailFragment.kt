package com.example.petsbnb.view

import android.app.Activity
import android.content.ContentValues
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.core.text.isDigitsOnly
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.data.model.ReviewModel
import com.example.petsbnb.databinding.FragmentReviewDetailfragmentBinding
import com.example.petsbnb.util.UiState
import com.example.petsbnb.util.isOnline
import com.example.petsbnb.util.toast
import com.example.petsbnb.util.toastShort
import com.example.petsbnb.viewmodel.ReviewViewModel
import dagger.hilt.android.AndroidEntryPoint
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

@AndroidEntryPoint
class ReviewDetailFragment: Fragment() {

    val TAG: String = "ReviewDetailFragment"
    lateinit var binding: FragmentReviewDetailfragmentBinding
    val viewModel: ReviewViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentReviewDetailfragmentBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.author.text = getAuthor()

        binding.create.setOnClickListener {

            if( isOnline( requireContext() ) == false ){
                toast("Not Internet conection. Connect to internet to add reviews")
            }
            else if(validation() && isOnline( requireContext() ) == true  ){
                viewModel.addReview(
                    ReviewModel(
                        id= "",
                        score = binding.score.text.toString(),
                        sitter = binding.sitter.text.toString(),
                        place = binding.place.text.toString(),
                        comment = binding.comment.text.toString(),
                        username = binding.author.text.toString(),
                        docURL= "https://cdn3.iconfinder.com/data/icons/avatars-9/145/Avatar_Dog-512.png"
                    )
                )

                viewModel.addReview.observe(viewLifecycleOwner){ state ->
                    when(state){
                        is UiState.Loading -> {
                            binding.status.text = "Loading..."
                        }

                        is UiState.Failure -> {
                            binding.status.text = "There was an error creating you review. Please try again"
                            toast(state.error)
                        }

                        is UiState.Success -> {
                            binding.status.text = "Review Created Successfully"

                            findNavController().navigate(R.id.action_reviewDetailfragment_to_reviewListingfragment)
                            // findNavController().navigate(R.id.action_petListingfragment_to_petDetailfragment)

                            toast(state.data)
                        }
                    }
                }
            }
        }

        //capturar imagen de la camara
        binding.photo.setOnClickListener {
            startForResult.launch(  Intent(MediaStore.ACTION_IMAGE_CAPTURE) )
            //findNavController().navigate(R.id.action_petDetailfragment_to_cameraXApp)
        }
    }

    private fun getAuthor(): String? {
        var msg = "Anonymous Author"
        val sharedPref = this.getActivity()?.getSharedPreferences(
            "username", Context.MODE_PRIVATE)
        if (sharedPref != null) {
            msg = sharedPref.getString("username", "").toString()
        }
        if (msg  != null) {
            return msg
        }else {
            return "Anonymous Author"
        }
    }


    private val startForResult = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
            result: ActivityResult ->
        if(result.resultCode == Activity.RESULT_OK){


            var intent = result.data
            var imageBitmap = intent?.extras?.get("data") as Bitmap
            //var imageView = binding.imgCam
            //imageView.setImageBitmap(imageBitmap)

            var f = saveMediaToStorage(imageBitmap)

            println(f)
        }
    }

    // validacion del formulario para agregar mascota
    private fun validation(): Boolean{

        var isValid = true

        if(binding.score.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("Enter Score")
            binding.status.text = "Enter Score"

        }else if( !binding.score.text.toString().isDigitsOnly() ){
            isValid = false
            toastShort("Score must be a number")
            binding.status.text = "Score must be a number"
        } else  if (Integer.parseInt(binding.score.text.toString()) < 1 || Integer.parseInt(binding.score.text.toString()) > 5){
            isValid = false
            toastShort("Score must be between 1 and 5")
            binding.status.text = "Score must be between 1 and 5"
        }
        else if(binding.sitter.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("Enter a sitter")
            binding.status.text = "Enter Sitter"
        }
        else if(binding.place.text.toString().isNullOrEmpty()){
            isValid = false
            toastShort("Enter a place")
            binding.status.text = "Enter Place"
        }

        return isValid
    }

    fun saveMediaToStorage(bitmap: Bitmap) {
        //Generating a file name
        val filename = "${System.currentTimeMillis()}.jpg"

        //Output stream
        var fos: OutputStream? = null

        //For devices running android >= Q
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            //getting the contentResolver
            context?.contentResolver?.also { resolver ->

                //Content resolver will process the contentvalues
                val contentValues = ContentValues().apply {

                    //putting file information in content values
                    put(MediaStore.MediaColumns.DISPLAY_NAME, filename)
                    put(MediaStore.MediaColumns.MIME_TYPE, "image/jpg")
                    put(MediaStore.MediaColumns.RELATIVE_PATH, Environment.DIRECTORY_PICTURES)
                }

                //Inserting the contentValues to contentResolver and getting the Uri
                val imageUri: Uri? =
                    resolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, contentValues)

                //Opening an outputstream with the Uri that we got
                fos = imageUri?.let { resolver.openOutputStream(it) }
            }
        } else {
            //These for devices running on android < Q
            //So I don't think an explanation is needed here
            val imagesDir =
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES)
            val image = File(imagesDir, filename)
            fos = FileOutputStream(image)
        }

        fos?.use {
            //Finally writing the bitmap to the output stream that we opened
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, it)
            // context?.toast("Saved to Photos")
        }
    }














}
