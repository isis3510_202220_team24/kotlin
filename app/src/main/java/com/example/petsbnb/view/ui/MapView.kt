package com.example.petsbnb.view.ui

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.petsbnb.R
import com.example.petsbnb.data.model.PlaceModel
import com.example.petsbnb.databinding.ActivityMapViewBinding
import com.example.petsbnb.databinding.FilterPlacesDialogLayoutBinding
import com.example.petsbnb.util.UiState
import com.example.petsbnb.viewmodel.MapViewModel
import com.example.petsbnb.viewmodel.PlaceAdapter
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationServices
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.GoogleMap.OnMyLocationButtonClickListener
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class MapView : AppCompatActivity(),
    OnMapReadyCallback, OnMyLocationButtonClickListener
     {

    private lateinit var mMap: GoogleMap
    private lateinit var binding: ActivityMapViewBinding
    private lateinit var dialogBinding: FilterPlacesDialogLayoutBinding
    //private lateinit var topMenuBinding: Men
    private lateinit var searchButton: FloatingActionButton
    private lateinit var filterButton: FloatingActionButton
    private val viewModel: MapViewModel by viewModels()
    // Default maps button to hide
    private lateinit var locationButton: View
    private lateinit var mapFragment: SupportMapFragment
    // The entry point to the Fused Location Provider.
    private lateinit var fusedLocationProviderClient: FusedLocationProviderClient
    private var lastKnownLocation: Location? = null
    private var currentLat: Double = 0.0
    private var currentLng: Double = 0.0

    private lateinit var localPlaces:MutableList<PlaceAdapter>
    private lateinit var markers:MutableMap<String, Marker>
    private var searchDistance: Double = -1.0

    companion object {
        const val REQUEST_CODE_LOCATION = 0
        private const val DEFAULT_ZOOM = 15f
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMapViewBinding.inflate((layoutInflater))
        val view = binding.root
        setContentView(view)

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        mapFragment = supportFragmentManager
            .findFragmentById(R.id.mMap) as SupportMapFragment
        mapFragment.getMapAsync(this)

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this)

    }

         /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        mMap.setMyLocationEnabled(true)

        locationButton = mapFragment.view!!.findViewById(0x2)
        if(locationButton != null)
            locationButton.setVisibility(View.GONE);

        localPlaces = mutableListOf()
        markers = mutableMapOf()

        searchButton = binding.searchButton
        searchButton.setOnClickListener {
            getDeviceLocation()
            chargePlaces()
        }

        filterButton = binding.filterButton
        filterButton.setOnClickListener {
            showFilterByDistanceDialog()
        }

        enableLocation()
    }

    private fun enableLocation() {
        if (!::mMap.isInitialized) return
        if (isLocationPermissionGranted())
            mMap.isMyLocationEnabled = true
        else
            requestLocationPermission()
    }


    private fun isLocationPermissionGranted() = ContextCompat.checkSelfPermission(
        this, Manifest.permission.ACCESS_FINE_LOCATION
    ) == PackageManager.PERMISSION_GRANTED


    private fun requestLocationPermission() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            )
        )
            Toast.makeText(
                this,
                "Ve a ajustes y activa los permisos de ubicación",
                Toast.LENGTH_SHORT
            ).show()
        else
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                REQUEST_CODE_LOCATION
            )
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_CODE_LOCATION -> if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                mMap.isMyLocationEnabled = true
            } else {
                Toast.makeText(
                    this,
                    "Ve a ajustes y activa los permisos de ubicación",
                    Toast.LENGTH_SHORT
                ).show()
            }
            else -> {}
        }
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        if (!::mMap.isInitialized) return
        if (!isLocationPermissionGranted()) {
            mMap.isMyLocationEnabled = false
            Toast.makeText(
                this,
                "ve a ajustes y activa los permisos de localización",
                Toast.LENGTH_SHORT
            ).show()
        }
    }

    override fun onMyLocationButtonClick(): Boolean {
        return false
    }


    @SuppressLint("MissingPermission")
    private fun getDeviceLocation() {
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         */
        try {
            if (isLocationPermissionGranted()) {
                val locationResult = fusedLocationProviderClient.lastLocation
                locationResult.addOnCompleteListener(this) { task ->
                    if (task.isSuccessful) {
                        // Set the map's camera position to the current location of the device.
                        lastKnownLocation = task.result
                        if (lastKnownLocation != null) {
                            currentLat = lastKnownLocation!!.latitude
                            currentLng = lastKnownLocation!!.longitude
                            mMap?.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                LatLng(currentLat, currentLng), DEFAULT_ZOOM))
                        }
                    } else {
                        Log.d("GET DEVICE LOC", "Current location is null. Using defaults.")
                        Log.e("GET DEVICE LOC", "Exception: %s", task.exception)
                        mMap?.moveCamera(CameraUpdateFactory
                            .newLatLngZoom(LatLng(4.603070, -74.065191), 20f))
                        mMap?.uiSettings?.isMyLocationButtonEnabled = false
                    }
                }
            }
        } catch (e: SecurityException) {
            Log.e("Exception: %s", e.message, e)
        }
    }

    private fun adaptPlaces(places: List<PlaceModel>) {
        /*if(localPlaces.size > 0) // clear places if is already filled
            localPlaces.clear()*/

        var nPlace = PlaceAdapter("", "","","",0,"", places[0].ubicacion, currentLat, currentLng)
        for(i in places.indices) {
            nPlace = PlaceAdapter(
                places[i].id, places[i].barrio, places[i].ciudad,
                places[i].description, places[i].precio, places[i].titulo, places[i].ubicacion,
                currentLat, currentLng)
            if (searchDistance == -1.0 ) // when the user haven't selected a filter
                localPlaces.add(nPlace)
            else{ // user have entered a filter
                if(searchDistance >= nPlace.distance)
                    localPlaces.add(nPlace)
            }
        }
    }

    private fun filterByDistance(){
        if (searchDistance > 0.0) { // user entered a valid distance
            var i = 0
            Log.d("KURO", "Entro al if de filterBydistance" + searchDistance.toString())
            Log.d("KURO", "Entro al if de filterBydistance " + localPlaces.size.toString())
            while (i < localPlaces.size) {
                    if (searchDistance < localPlaces[i].distance) {
                        Log.d("KURO", "Entro al if de borrado " + searchDistance.toString())
                        Log.d("KURO", "Entro al if de borrado2 " + localPlaces.size.toString())
                        deleteMarker(localPlaces[i].id)
                    }
                    i++
            }
        }
    }

    private fun showFilterByDistanceDialog() {
        dialogBinding = FilterPlacesDialogLayoutBinding.inflate(layoutInflater)
        val builder = AlertDialog.Builder(this)

        with(builder){
            setTitle("Filter sitter by distance")
            setPositiveButton("Ok"){ dialog, which ->
                val newDistance = dialogBinding.distanceInputDialog.text.toString().toDouble()
                if(searchDistance < newDistance && searchDistance > 0.0) // not first filter and new values required
                {
                    Log.d("KURO showFilter", "Entró al if de recargar")
                    drawMarkers()
                }
                searchDistance = newDistance
                Log.d("KURO", "new searchDistance from input "+searchDistance.toString())
                filterByDistance()
            }
            setNegativeButton("Cancel"){ dialog, which ->
                Log.d("Filter input dialog", "Negative button clicked")
            }
            setView(dialogBinding.root)
            show()
        }
    }

    private fun drawMarkers() {
        Log.d("KURO drawMarkers", localPlaces.size.toString())

            var marker: Marker? = null
            for (i in localPlaces.indices) {
                marker = mMap.addMarker(
                    MarkerOptions().position(
                        LatLng(
                            localPlaces[i].ubicacion["latitud"]!!,
                            localPlaces[i].ubicacion["longitud"]!!
                        )
                    )
                        .title(localPlaces[i].titulo)
                        .snippet(
                            "Precio: $" + localPlaces[i].precio.toString() +
                                    " distancia: " + localPlaces[i].distance.toString() + "km"
                        )
                )
                markers.put(localPlaces[i].id, marker!!)
            }
    }

    private fun deleteMarker(id:String){
        var toDelete = markers.get(id)
        if (toDelete != null){
            toDelete.remove()
            markers.remove(id)
        }
    }


    private fun chargePlaces() {
        Log.d("KURO charge places", "entró al metodo")
        viewModel.getPlaces()
        viewModel.placeModel.observe(this, Observer { state ->
            when (state) {
                is UiState.Loading -> {
                    Log.d("KURO charge places", "UI state Loading")
                    Toast.makeText(
                        this,
                        "Cargando lugares",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is UiState.Failure -> {
                    Log.d("KURO charge places", "UI state Failure")
                    Toast.makeText(
                        this,
                        "Verifica tu conexión y vuelve a intentarlo",
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is UiState.Success -> {
                    Log.d("KURO charge places", "UI state Success")
                    adaptPlaces(state.data)
                    drawMarkers()

                    Log.d("KURO charge places", localPlaces.size.toString())
                }
            }
        })

    }
}