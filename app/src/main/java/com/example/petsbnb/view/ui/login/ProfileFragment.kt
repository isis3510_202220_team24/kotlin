package com.example.petsbnb.view.ui.login

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import com.example.petsbnb.R
import com.example.petsbnb.databinding.FragmentProfileBinding
import com.example.petsbnb.view.ui.auth.AuthViewModel
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class ProfileFragment : Fragment() {

    val TAG: String = "Profilefragment"
    lateinit var binding: FragmentProfileBinding
    val viewModel: AuthViewModel by viewModels()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentProfileBinding.inflate(layoutInflater)
        return binding.root
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {

        super.onViewCreated(view, savedInstanceState)

        binding.reviews.setOnClickListener {
            findNavController().navigate(R.id.action_ProfileFragment_to_reviewsListingFragment)
        }
        binding.forgot.setOnClickListener {
            findNavController().navigate(R.id.action_ProfileFragment_to_forgotFragment)
        }
        binding.edit.setOnClickListener {
            findNavController().navigate(R.id.action_ProfileFragment_to_userEdit)
        }
        fillUserData()

    }

     fun getUser(): String {
        return viewModel.getCurrentUser()
    }

     fun fillUserData() {

        var msg = "Mail: " +  getUser()
        binding.username.text = msg.toString()

    }
}