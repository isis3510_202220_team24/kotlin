package com.example.petsbnb.data.model


import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ReservaModel(
    var comment: String = "",
    val cost: Int = 0,
    val finishDate: String = "",
    val initialDate: String = "",
    val multipleDays: Boolean=false
): Parcelable
