package com.example.petsbnb.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PlaceModel (
    var id: String = "",
    val barrio : String = "",
    val ciudad : String = "",
    val description: String = "",
    val precio: Int = 0,
    val titulo: String ="",
    val ubicacion: MutableMap<String, Double> = mutableMapOf<String, Double>()
    // val imagenes: MutableMap<Int, String> = mutableMapOf<Int, String>()

):Parcelable