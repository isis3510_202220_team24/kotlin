package com.example.petsbnb.data.repository

import android.net.Uri
import com.example.petsbnb.data.model.ReviewModel
import com.example.petsbnb.util.FireStoreTables
import com.example.petsbnb.util.UiState
import com.google.firebase.FirebaseException
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext


class ReviewRepositoryImp (
    val database: FirebaseFirestore,
    val storageReference: StorageReference
): ReviewRepository {

    var cacheReview = SimpleReviewCache.reviewMap

    override fun getReviews(result: (UiState<List<ReviewModel>>) -> Unit){

        if( cacheReview.isNotEmpty() ){

            val reviewCa = arrayListOf<ReviewModel>()

            for ( (key, value) in cacheReview) {
                reviewCa.add(value)
            }

            result.invoke(
                UiState.Success(reviewCa)
            )

        }
        else{
            database.collection(FireStoreTables.REVIEW)
                .get()
                .addOnSuccessListener {

                    val reviews = arrayListOf<ReviewModel>()

                    for(document in it){
                        val rev = document.toObject(ReviewModel::class.java)
                        var revId = document.id
                        reviews.add(rev)

                        cacheReview[revId.toString()] = rev
                    }
                    result.invoke(
                        UiState.Success(reviews)
                    )
                }
                .addOnFailureListener{
                    result.invoke(
                        UiState.Failure(
                            it.localizedMessage
                        )
                    )
                }
        }
    }




    override fun addReview(reviewModel: ReviewModel, result: (UiState<String>) -> Unit) {

        val document = database.collection(FireStoreTables.REVIEW).document()
        reviewModel.id = document.id

        document
            .set(reviewModel)
            .addOnSuccessListener {
                cacheReview.clear()

                result.invoke(
                    UiState.Success("The Review has been create succesfully")
                )
            }
            .addOnFailureListener {
                result.invoke(
                    UiState.Failure(it.localizedMessage)
                )
            }
    }

    override suspend fun uploadSingleFile(fileUri: Uri, oneResult: (UiState<Uri>) -> Unit) {

        try{
            val uri: Uri = withContext(Dispatchers.Default){
                storageReference
                    .putFile(fileUri)
                    .await()
                    .storage
                    .downloadUrl
                    .await()
            }
            oneResult.invoke(UiState.Success(uri))
        } catch (e: FirebaseException){
            oneResult.invoke(UiState.Failure(e.message))
        }catch (e: Exception){
            oneResult.invoke(UiState.Failure(e.message))
        }
    }




    override  fun updateReview(reviewModel: ReviewModel, result: (UiState<String>) -> Unit){

        val document = database.collection(FireStoreTables.REVIEW).document(reviewModel.id) //cambiar por el id de la mascota
        document
            .set(reviewModel)
            .addOnSuccessListener{
                cacheReview.clear()
                result.invoke(
                    UiState.Success("Review Updated succesfully")
                )
            }
            .addOnFailureListener{
                result.invoke(
                    UiState.Failure(
                        it.localizedMessage
                    )
                )
            }

    }






    override  fun deleteReview(reviewModel: ReviewModel, result: (UiState<String>) -> Unit){

        val document = database.collection(FireStoreTables.REVIEW).document(reviewModel.id) //cambiar por el id de la mascota
        document
            .delete()
            .addOnSuccessListener{

                cacheReview.clear()

                result.invoke(
                    UiState.Success("Review Delete OK")
                )
            }
            .addOnFailureListener{
                result.invoke(
                    UiState.Failure(
                        it.localizedMessage
                    )
                )
            }


    }




}