package com.example.petsbnb.data.repository

import android.net.Uri
import com.example.petsbnb.data.model.PlaceModel
import com.example.petsbnb.util.UiState

interface PlaceRepository {

    fun getPlaces(result: (UiState<List<PlaceModel>>) -> Unit)
    fun addPlace(placeModel: PlaceModel, result: (UiState<String>) -> Unit )
    fun updatePlace(placeModel: PlaceModel, result: (UiState<String>) -> Unit)
    fun deletePlace(place : PlaceModel, result: (UiState<String>) -> Unit)
    // For images
    suspend fun uploadSingleFile(fileUri: Uri, oneResult: (UiState<Uri>) -> Unit)
}