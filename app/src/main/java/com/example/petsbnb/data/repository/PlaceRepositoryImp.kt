package com.example.petsbnb.data.repository

import android.net.Uri
import com.example.petsbnb.data.model.PlaceModel
import com.example.petsbnb.data.repository.PlacesCache.placesMap
import com.example.petsbnb.util.FireStoreTables
import com.example.petsbnb.util.UiState
import com.google.firebase.FirebaseException
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class PlaceRepositoryImp(val database: FirebaseFirestore, val storageReference: StorageReference):PlaceRepository {

    var cachePlaces = placesMap


    override fun getPlaces (result: (UiState<List<PlaceModel>>) -> Unit){

        if( cachePlaces.isNotEmpty() ){

            val placesCa = arrayListOf<PlaceModel>()

            for ( (key, value) in cachePlaces) {
                placesCa.add(value)
            }
            result.invoke(
                UiState.Success(placesCa)
            )
        }
        else{
            database.collection(FireStoreTables.PLACE)
                .get()
                .addOnSuccessListener {

                    val places = arrayListOf<PlaceModel>()
                    for(document in it){
                        val place = document.toObject(PlaceModel::class.java)
                        var placeId = document.id
                        places.add(place)

                        cachePlaces[placeId.toString()] = place
                    }
                    result.invoke(
                        UiState.Success(places)
                    )
                }
                .addOnFailureListener{
                    result.invoke(
                        UiState.Failure(it.localizedMessage)
                    )
                }
        }
    }


    override fun addPlace(placeModel: PlaceModel, result: (UiState<String>) -> Unit) {

        val document = database.collection(FireStoreTables.PLACE).document()
        placeModel.id = document.id

        document
            .set(placeModel)
            .addOnSuccessListener {
                cachePlaces.clear() //limpia el cache

                result.invoke(
                    UiState.Success("The Place  has been create succesfully")
                )
            }
            .addOnFailureListener {
                result.invoke(
                    UiState.Failure(it.localizedMessage)
                )
            }
    }


    override suspend fun uploadSingleFile(fileUri: Uri, oneResult: (UiState<Uri>) -> Unit) {

        try{
            val uri: Uri = withContext(Dispatchers.IO){
                storageReference
                    .putFile(fileUri)
                    .await()
                    .storage
                    .downloadUrl
                    .await()
            }
            oneResult.invoke(UiState.Success(uri))
        } catch (e: FirebaseException){
            oneResult.invoke(UiState.Failure(e.message))
        }catch (e: Exception){
            oneResult.invoke(UiState.Failure(e.message))
        }
    }


    override  fun updatePlace(place: PlaceModel, result: (UiState<String>) -> Unit){

        val document = database.collection(FireStoreTables.PLACE).document(place.id) //cambiar por el id de la mascota
        document
            .set(place)
            .addOnSuccessListener{
                cachePlaces.clear() //limpia el cache

                result.invoke(
                    UiState.Success("Place Update succesfully")
                )
            }
            .addOnFailureListener{
                result.invoke(
                    UiState.Failure(
                        it.localizedMessage
                    )
                )
            }
    }


    override  fun deletePlace(place: PlaceModel, result: (UiState<String>) -> Unit){

        val document = database.collection(FireStoreTables.PLACE).document(place.id) //cambiar por el id de la mascota
        document
            .delete()
            .addOnSuccessListener{

                cachePlaces.clear() //limpia el cache

                result.invoke(
                    UiState.Success("Place Delete OK")
                )
            }
            .addOnFailureListener{
                result.invoke(
                    UiState.Failure(
                        it.localizedMessage
                    )
                )
            }
    }

}