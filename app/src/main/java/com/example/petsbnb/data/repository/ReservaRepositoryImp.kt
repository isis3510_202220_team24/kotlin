package com.example.petsbnb.data.repository

import androidx.core.util.isNotEmpty
import com.example.petsbnb.data.model.ReservaModel
import com.example.petsbnb.data.repository.ReservaCache.reservasSparseArray
import com.example.petsbnb.util.FireStoreTables
import com.example.petsbnb.util.UiState
import com.google.firebase.firestore.FirebaseFirestore

class ReservaRepositoryImp(
    val database: FirebaseFirestore
): ReservaRepository {

    var cacheReservas = reservasSparseArray


    override fun addReserva(reservaModel: ReservaModel, result: (UiState<String>) -> Unit) {

        val document = database.collection(FireStoreTables.RESERVA).document()
        //reservaModel.id = document.id

        document
            .set(reservaModel)
            .addOnSuccessListener {

                result.invoke(
                    UiState.Success("The Reserva has been create succesfully")
                )
            }
            .addOnFailureListener {
                result.invoke(
                    UiState.Failure(it.localizedMessage)
                )
            }


    }






    override fun getReservas(result: (UiState<List<ReservaModel>>) -> Unit) {

        if( cacheReservas.isNotEmpty() ){

            val reservaCa = arrayListOf<ReservaModel>()

            for (i in 0 until cacheReservas.size()) {
                val key: Int = cacheReservas.keyAt(i)
                // get the object by the key.
                val obj = cacheReservas.get(key)

                reservaCa.add(obj)
            }
            result.invoke(
                UiState.Success(reservaCa)
            )
        }
        else {

            var iCount = 0

            database.collection(FireStoreTables.RESERVA)
                .get()
                .addOnSuccessListener {
                    val reservas = arrayListOf<ReservaModel>()

                    for(document in it){
                        val reserva = document.toObject(ReservaModel::class.java)
                        reservas.add(reserva)

                        cacheReservas.append(iCount,reserva)

                        iCount = iCount+1


                    }
                    result.invoke(
                        UiState.Success(reservas)
                    )
                }
                .addOnFailureListener{
                    result.invoke(
                        UiState.Failure(
                            it.localizedMessage
                        )
                    )
                }









        }











    }








}