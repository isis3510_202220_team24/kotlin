package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.SitterModel
import com.example.petsbnb.util.UiState

interface SittersRepository {

    fun getSitters(result: (UiState<List<SitterModel>>) -> Unit)

}