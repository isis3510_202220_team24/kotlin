package com.example.petsbnb.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class UserModel(
    var id: String ="",
    val age: Int = 0,
    val name: String = "",
    val username: String ="",
    val ocupation: String = "",
    val photoURL: String=""

): Parcelable