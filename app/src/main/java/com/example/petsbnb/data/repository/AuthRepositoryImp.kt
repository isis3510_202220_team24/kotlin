package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.UserModel
import com.example.petsbnb.util.FireStoreTables
import com.example.petsbnb.util.UiState
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException
import com.google.firebase.auth.FirebaseAuthUserCollisionException
import com.google.firebase.auth.FirebaseAuthWeakPasswordException
import com.google.firebase.firestore.FirebaseFirestore


class AuthRepositoryImp(
    val auth: FirebaseAuth,
    val database: FirebaseFirestore
) : AuthRepository {

    override fun register(
        email: String,
        password: String,
        user: UserModel, result: (UiState<String>) -> Unit)

    {
        auth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener {
                if (it.isSuccessful){
                    updateUserInfo(user) { state ->
                        when(state){
                            is UiState.Success -> {
                                result.invoke(
                                    UiState.Success("User register successfully!")
                                )
                            }
                            is UiState.Failure -> {
                                result.invoke(UiState.Failure(state.error))
                            }
                        }
                    }
                }else{
                    try {
                        throw it.exception ?: java.lang.Exception("Invalid authentication")
                    } catch (e: FirebaseAuthWeakPasswordException) {
                        result.invoke(UiState.Failure("Authentication failed, Password should be at least 6 characters"))
                    } catch (e: FirebaseAuthInvalidCredentialsException) {
                        result.invoke(UiState.Failure("Authentication failed, Invalid email entered"))
                    } catch (e: FirebaseAuthUserCollisionException) {
                        result.invoke(UiState.Failure("Authentication failed, Email already registered."))
                    } catch (e: Exception) {
                        result.invoke(UiState.Failure(e.message))
                    }
                }
            }
            .addOnFailureListener {
                result.invoke(
                    UiState.Failure(
                        it.localizedMessage
                    )
                )
            }
    }

    override fun login(
        email: String,
        password: String,
        result: (UiState<String>) -> Unit) {
        auth.signInWithEmailAndPassword(email,password)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    //val prefs: Prefs by lazy {
                    //    App.prefs!!
                    //}
                    //prefs.username = email

                    result.invoke(UiState.Success("Login successfully!"))
                }
            }.addOnFailureListener {
                result.invoke(UiState.Failure("Authentication failed, Check email and password"))
            }
    }


    override fun logout(result: () -> Unit) {
        auth.signOut()
        //appPreferences.edit().putString(SharedPrefConstants.USER_SESSION,null).apply()
        result.invoke()
    }

    override fun forgot(email: String, result: (UiState<String>) -> Unit) {
        auth.sendPasswordResetEmail(email)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    result.invoke(UiState.Success("Email has been sent"))

                } else {
                    result.invoke(UiState.Failure(task.exception?.message))
                }
            }.addOnFailureListener {
                result.invoke(UiState.Failure("Authentication failed, Check email"))
            }
    }


    override fun getCurrentUserMail(): String {
        val user = FirebaseAuth.getInstance().currentUser
        var userEmail: String? = null
        if (user != null) {
            userEmail = user.email
        } else {
            userEmail=  null
        }
        return userEmail.toString()
    }


    override fun getName(): String {
        var res = ""
        database.collection(FireStoreTables.USERPBNB).whereEqualTo("username", getCurrentUserMail()).get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    res =  document.get("name").toString()
                }
            }
            .addOnFailureListener {
                res = "N/A"
            }
        return res
    }

    override fun getOcupation(): String {
        var res = ""
        val docRef = database.collection(FireStoreTables.USERPBNB).whereEqualTo("username", getCurrentUserMail()).get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    res =  document.get("ocupation").toString()
                }
            }
            .addOnFailureListener { exception ->
                res = "N/A"
            }
        return res
     }

    override fun getAge(): String {
        var res = ""
        database.collection(FireStoreTables.USERPBNB).whereEqualTo("username", getCurrentUserMail()).get()
            .addOnSuccessListener { documents ->
                for (document in documents) {
                    res =  document.get("age").toString()
                }

            }
            .addOnFailureListener {
                res = "N/A"
            }
        return res
    }

    fun updateUserInfo(user: UserModel, result: (UiState<String>) -> Unit) {

        val document = database.collection(FireStoreTables. USERPBNB).document()
        user.id = document.id


        document
            .set(user)
            .addOnSuccessListener {

                result.invoke(
                    UiState.Success("The User  has been create succesfully")
                )
            }
            .addOnFailureListener {
                result.invoke(
                    UiState.Failure("The User has not been created")
                )
            }
    }

    //override suspend fun uploadSingleFile(fileUri: Uri, oneResult: (UiState<Uri>) -> Unit) {
    //    try{
    //        val uri: Uri = withContext(Dispatchers.IO){
    //            storageReference
    //                .putFile(fileUri)
    //                .await()
    //                .storage
    //                .downloadUrl
    //                .await()
    //        }
    //        oneResult.invoke(UiState.Success(uri))
    //    } catch (e: FirebaseException){
    //        oneResult.invoke(UiState.Failure(e.message))
    //    }catch (e: Exception){
    //        oneResult.invoke(UiState.Failure(e.message))
    //    }
    //}
}

