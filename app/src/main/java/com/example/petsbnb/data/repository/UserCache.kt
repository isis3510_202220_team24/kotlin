package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.UserModel

object UserCache {

    val currentUser =mutableListOf<UserModel>()

    fun add(user: UserModel){
        currentUser[0] = user
    }
    fun set(user: UserModel){
        currentUser[0] = user
    }
    fun get(): UserModel {
        return currentUser[0]
    }

}