package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.ReviewModel

object SimpleReviewCache {
    val reviewMap = mutableMapOf<String, ReviewModel>()
}