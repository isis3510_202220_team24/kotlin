package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.PlaceModel

object PlacesCache {
    val placesMap = mutableMapOf<String, PlaceModel>()
}