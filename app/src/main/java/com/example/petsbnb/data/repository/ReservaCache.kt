package com.example.petsbnb.data.repository

import android.util.SparseArray
import com.example.petsbnb.data.model.ReservaModel

object ReservaCache {

    val reservasSparseArray = SparseArray<ReservaModel>()

}