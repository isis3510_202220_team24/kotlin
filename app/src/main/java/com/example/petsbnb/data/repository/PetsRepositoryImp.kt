package com.example.petsbnb.data.repository

import android.net.Uri
import androidx.core.util.isNotEmpty
import com.example.petsbnb.data.model.PetModel
import com.example.petsbnb.data.repository.PetsCacheNew.petsArray
import com.example.petsbnb.data.repository.SimplePetsCache.petsMap
import com.example.petsbnb.util.FireStoreTables
import com.example.petsbnb.util.UiState
import com.google.firebase.FirebaseException
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.StorageReference
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.tasks.await
import kotlinx.coroutines.withContext

class PetsRepositoryImp(
    val database: FirebaseFirestore,
    val storageReference: StorageReference
): PetsRepository{


      var cachePets = petsMap
    //var cachePets = petsArray


    override fun getPets(result: (UiState<List<PetModel>>) -> Unit){

        if( cachePets.isNotEmpty() ){

            val petsCa = arrayListOf<PetModel>()

            for ( (key, value) in cachePets) {
                petsCa.add(value)
            }

        //  for (i in 0 until cachePets.size()) {
        //        val key: Int = cachePets.keyAt(i)
        //        // get the object by the key.
        //        val obj = cachePets.get(key)
        //
        //        petsCa.add(obj)
        //    }

            result.invoke(
                UiState.Success(petsCa)
            )

        }
        else{

        //    var iCount = 0

            database.collection(FireStoreTables.PET)
                .get()
                .addOnSuccessListener {

                    val pets = arrayListOf<PetModel>()

                    for(document in it){
                        val pet = document.toObject(PetModel::class.java)
                        var petId = document.id
                        pets.add(pet)


                        cachePets[petId.toString()] = pet

                     //   cachePets.append(iCount,pet)
                     //   iCount = iCount+1


                    }
                    result.invoke(
                        UiState.Success(pets)
                    )
                }
                .addOnFailureListener{
                    result.invoke(
                        UiState.Failure(
                            it.localizedMessage
                        )
                    )
                }


        }






    }




    override fun addPet(petModel: PetModel, result: (UiState<String>) -> Unit) {

        val document = database.collection(FireStoreTables.PET).document()
        petModel.id = document.id

        document
            .set(petModel)
            .addOnSuccessListener {
                cachePets.clear() //limpia el cache

                result.invoke(
                    UiState.Success("The Pet  has been create succesfully")
                )
            }
            .addOnFailureListener {
                result.invoke(
                    UiState.Failure(it.localizedMessage)
                )
            }
    }




    override suspend fun uploadSingleFile(fileUri: Uri, oneResult: (UiState<Uri>) -> Unit) {

        try{
            val uri: Uri = withContext(Dispatchers.IO){
                storageReference
                    .putFile(fileUri)
                    .await()
                    .storage
                    .downloadUrl
                    .await()
            }
            oneResult.invoke(UiState.Success(uri))
        } catch (e: FirebaseException){
            oneResult.invoke(UiState.Failure(e.message))
        }catch (e: Exception){
            oneResult.invoke(UiState.Failure(e.message))
        }



    }




    override  fun updatePet(pet: PetModel, result: (UiState<String>) -> Unit){

        val document = database.collection(FireStoreTables.PET).document(pet.id) //cambiar por el id de la mascota
        document
            .set(pet)
            .addOnSuccessListener{

                cachePets.clear() //limpia el cache


                result.invoke(
                    UiState.Success("Pet Update succesfully")
                )
            }
            .addOnFailureListener{
                result.invoke(
                    UiState.Failure(
                        it.localizedMessage
                    )
                )
            }

    }






    override  fun deletePet(pet: PetModel, result: (UiState<String>) -> Unit){

        val document = database.collection(FireStoreTables.PET).document(pet.id) //cambiar por el id de la mascota
        document
            .delete()
            .addOnSuccessListener{

                cachePets.clear() //limpia el cache

                result.invoke(
                    UiState.Success("Pet Delete OK")
                )
            }
            .addOnFailureListener{
                result.invoke(
                    UiState.Failure(
                        it.localizedMessage
                    )
                )
            }


    }








}