package com.example.petsbnb.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class ReviewModel (var id: String ="",
                        val username: String = "",
                        val score: String = "",
                        val sitter: String = "",
                        val place: String ="",
                        val comment: String = "",
                        val docURL: String="",
): Parcelable