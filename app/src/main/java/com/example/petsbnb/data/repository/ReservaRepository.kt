package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.ReservaModel
import com.example.petsbnb.util.UiState

interface ReservaRepository {

    fun addReserva(reservaModel: ReservaModel, result: (UiState<String>) -> Unit)
    fun getReservas(result: (UiState<List<ReservaModel>>) -> Unit)


}