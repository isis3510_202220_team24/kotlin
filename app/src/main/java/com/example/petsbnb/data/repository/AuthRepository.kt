package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.UserModel
import com.example.petsbnb.util.UiState

interface AuthRepository {
    fun login(email: String, password: String, result: (UiState<String>) -> Unit)
    fun register(email: String, password: String,user:UserModel, result: (UiState<String>) -> Unit)
    fun logout(result: () -> Unit)
    fun forgot(email: String, result: (UiState<String>) -> Unit)
    fun getCurrentUserMail():String

    fun getName():String
    fun getAge():String
    fun getOcupation():String
}