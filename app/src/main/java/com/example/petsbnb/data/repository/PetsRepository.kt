package com.example.petsbnb.data.repository

import android.net.Uri
import com.example.petsbnb.data.model.PetModel
import com.example.petsbnb.util.UiState

interface PetsRepository {

    fun getPets(result: (UiState<List<PetModel>>) -> Unit)
    fun addPet(petModel: PetModel, result: (UiState<String>) -> Unit )
    fun updatePet(petModel: PetModel,  result: (UiState<String>) -> Unit )
    fun deletePet(pet: PetModel,result: (UiState<String>) -> Unit)

    //subir imagenes a fireStorage
    suspend fun uploadSingleFile(fileUri: Uri, oneResult: (UiState<Uri>) -> Unit)

}