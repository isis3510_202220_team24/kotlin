package com.example.petsbnb.data.repository

import com.example.petsbnb.data.model.SitterModel

object SittersCacheHash {

    val sittersMap = mutableMapOf<String, SitterModel>()

}