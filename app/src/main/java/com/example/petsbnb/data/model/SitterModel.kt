package com.example.petsbnb.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class SitterModel (
    //val age: String = "",
    val age: Int = 0,
    val name: String = "",
    val pictureUrl: String ="",
    val rating: Int = 0,
    val space: String = ""
): Parcelable