package com.example.petsbnb.data.repository

import android.content.Context
import android.content.SharedPreferences

class Prefs (context: Context) {
    private val preferences: SharedPreferences = context.getSharedPreferences("user", Context.MODE_PRIVATE)

    private var NAME_KEY = "name"
    private var USERNAME_KEY = "username"

    var name: String?
        get() = preferences.getString(NAME_KEY, "")
        set(value) = preferences.edit().putString(NAME_KEY, value).apply()

    var username: String?
        get() = preferences.getString(USERNAME_KEY, "")
        set(value) = preferences.edit().putString(USERNAME_KEY, value).apply()
}