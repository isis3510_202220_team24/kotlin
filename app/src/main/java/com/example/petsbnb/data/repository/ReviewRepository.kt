package com.example.petsbnb.data.repository

import android.net.Uri
import com.example.petsbnb.data.model.ReviewModel
import com.example.petsbnb.util.UiState

interface ReviewRepository {

    fun getReviews(result: (UiState<List<ReviewModel>>) -> Unit)
    fun addReview(reviewModel: ReviewModel, result: (UiState<String>) -> Unit )
    fun updateReview(reviewModel: ReviewModel, result: (UiState<String>) -> Unit )
    fun deleteReview(reviewModel: ReviewModel, result: (UiState<String>) -> Unit )
    suspend fun uploadSingleFile(fileUri: Uri, oneResult: (UiState<Uri>) -> Unit)
}