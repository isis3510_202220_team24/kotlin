package com.example.petsbnb.data.repository

import androidx.core.util.isNotEmpty
import com.example.petsbnb.data.model.SitterModel
import com.example.petsbnb.data.repository.SittersCache.sittersSparseArray
import com.example.petsbnb.data.repository.SittersCacheHash.sittersMap
import com.example.petsbnb.util.FireStoreTables
import com.example.petsbnb.util.UiState
import com.google.firebase.firestore.FirebaseFirestore

class SittersRepositoryImp(
    val database: FirebaseFirestore
): SittersRepository {

    //var cacheSitters = sittersSparseArray
    var cacheSitters = sittersMap

    override fun getSitters(result: (UiState<List<SitterModel>>) -> Unit) {


        if( cacheSitters.isNotEmpty() ){

            val sittersCa = arrayListOf<SitterModel>()

       //     for (i in 0 until cacheSitters.size()) {
       //         val key: Int = cacheSitters.keyAt(i)
       //         // get the object by the key.
       //         val obj = cacheSitters.get(key)
       //         sittersCa.add(obj)
       //     }

            for ( (key, value) in cacheSitters) {
                sittersCa.add(value)
            }




            result.invoke(
                UiState.Success(sittersCa)
            )
        }
        else{

          //  var iCount = 0

            database.collection(FireStoreTables.SITTER)
                .get()
                .addOnSuccessListener {

                    val sitters = arrayListOf<SitterModel>()

                    for(document in it){
                        val sitter = document.toObject(SitterModel::class.java)
                        var sitterId = document.id



                        sitters.add(sitter)

                    //    cacheSitters.append(iCount,sitter)
                    //    iCount = iCount+1


                        cacheSitters[sitterId.toString()] = sitter



                    }

                    result.invoke(
                        UiState.Success(sitters)
                    )
                }
                .addOnFailureListener{
                    result.invoke(
                        UiState.Failure(
                            it.localizedMessage
                        )
                    )
                }


        }








    }




}