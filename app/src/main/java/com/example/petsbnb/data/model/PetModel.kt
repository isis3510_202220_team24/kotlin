package com.example.petsbnb.data.model

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class PetModel(
    var id: String = "",
    val age: String = "",
    val name: String="",
    val needs: Boolean=false,
    val photoURL: String="",
    val race: String="",

    //val ownerUsername:String=""
): Parcelable