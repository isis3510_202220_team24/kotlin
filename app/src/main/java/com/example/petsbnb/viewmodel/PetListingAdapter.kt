package com.example.petsbnb.viewmodel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.petsbnb.data.model.PetModel
import com.example.petsbnb.databinding.ItemPetLayoutBinding
import com.squareup.picasso.Picasso



class PetListingAdapter(
    val onItemClicked: (Int,PetModel) -> Unit,
    val onEditClicked: (Int,PetModel) -> Unit,
    val onDeleteClicked: (Int,PetModel) -> Unit
): RecyclerView.Adapter<PetListingAdapter.MyViewHolder>(){

    private var list: MutableList<PetModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder{
        val itemView = ItemPetLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]
        holder.bind(item)
    }

    fun updateList(list: MutableList<PetModel>){
        this.list = list
        notifyDataSetChanged()
    }



    fun removeItem(position: Int){
        list.removeAt(position)
        notifyItemChanged(position)
    }


    override fun getItemCount(): Int {
        return list.size
    }



    inner class MyViewHolder(val binding: ItemPetLayoutBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: PetModel){
            binding.txtNamePet.setText(item.name)
            binding.txtAgePet.setText(item.age)
            binding.txtRacePet.setText(item.race)
            binding.txtNeedsPet.setText(item.needs.toString())

            //imagen et
            var imgCont  = binding.imgPet
            Picasso.get().load(item.photoURL.toString()).resize(150,150).into(imgCont)

            binding.btnEdit.setOnClickListener { onEditClicked.invoke(adapterPosition,item) }
            binding.btnDeletePet.setOnClickListener { onDeleteClicked.invoke(adapterPosition,item) }
            binding.itemLayout.setOnClickListener { onItemClicked.invoke(adapterPosition, item) }
        }
    }







        }