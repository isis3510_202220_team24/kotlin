package com.example.petsbnb.viewmodel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.petsbnb.data.model.SitterModel
import com.example.petsbnb.databinding.ItemSitterLayoutBinding
import com.squareup.picasso.Picasso

class SitterListingAdapter(
    val onItemClicked: (Int,SitterModel) -> Unit,
    val onReservaClicked: (Int,SitterModel) -> Unit
): RecyclerView.Adapter<SitterListingAdapter.MyViewHolder>() {

    private var list: MutableList<SitterModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = ItemSitterLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
       val item = list[position]
        holder.bind(item)
    }

    fun updateList(List: MutableList<SitterModel>){
        this.list = List
        notifyDataSetChanged()
    }


    fun removeItem(position: Int){
        list.removeAt(position)
        notifyItemChanged(position)
    }


    override fun getItemCount(): Int {
        return list.size
    }




    inner class MyViewHolder(val binding: ItemSitterLayoutBinding ) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: SitterModel){
            binding.txtNameSitter.setText(item.name)
            binding.txtRatingSitter.setText(item.rating.toString())
            binding.txtSpaceSitter.setText(item.space)
            //binding.txtAgeSitter.setText(item.age)}
            binding.txtAgeSitter.setText(item.age.toString())


        //imagen en contenedor
            var imgCont = binding.imgSitter
            Picasso.get().load(item.pictureUrl.toString()).resize(150,150).into(imgCont)


            binding.btnReserveSitter.setOnClickListener{ onReservaClicked.invoke(adapterPosition,item) }

            binding.itemLayoutSitters.setOnClickListener{ onItemClicked.invoke(adapterPosition,item) }
        }
    }





}