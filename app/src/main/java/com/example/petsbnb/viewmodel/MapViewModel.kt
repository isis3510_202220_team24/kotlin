package com.example.petsbnb.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.petsbnb.data.model.PlaceModel
import com.example.petsbnb.data.repository.PlaceRepository
import com.example.petsbnb.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class MapViewModel @Inject constructor(val repository : PlaceRepository ) : ViewModel(){

    private val _oneDone = MutableLiveData<Boolean>()

    private val _places = MutableLiveData< UiState<List<PlaceModel>> >()

    val placeModel: LiveData< UiState<List<PlaceModel>> >
        get() = _places


    private val _addPlace = MutableLiveData< UiState<String> >()
    val addPlace: LiveData< UiState< String > >
        get() = _addPlace


    private val _updatePlace = MutableLiveData<UiState<String>>()
    val updatePlace: LiveData<UiState<String>>
        get() = _updatePlace

    private val _deletePlace = MutableLiveData<UiState<String>>()
    val deletePlace: LiveData<UiState<String>>
        get() = _deletePlace

    fun getPlaces(){
        _places.value = UiState.Loading
        repository.getPlaces { _places.value = it }
    }

    fun addPlace(place: PlaceModel){
        _addPlace.value = UiState.Loading
        repository.addPlace(place){ _addPlace.value = it }
    }

    fun updatePlace(place: PlaceModel){
        _updatePlace.value = UiState.Loading
        repository.updatePlace(place){_updatePlace.value = it}
    }

    fun deletePlace(place: PlaceModel){
        _deletePlace.value = UiState.Loading
        repository.deletePlace(place) {_deletePlace.value = it}

    }

    fun onUploadSingleFile(fileUris: Uri, onResult: (UiState<Uri>) -> Unit){
        onResult.invoke(UiState.Loading)
        viewModelScope.launch {
            repository.uploadSingleFile(fileUris,onResult)
        }
    }
}
