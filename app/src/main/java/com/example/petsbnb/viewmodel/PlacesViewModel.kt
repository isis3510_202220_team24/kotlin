package com.example.petsbnb.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.petsbnb.data.model.PlaceModel
import com.example.petsbnb.data.repository.PlaceRepository
import com.example.petsbnb.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class PlacesViewModel @Inject constructor(
    val repository: PlaceRepository
): ViewModel() {

    private val _places = MutableLiveData<UiState<List<PlaceModel>>>()
    val PlaceModel: LiveData< UiState<List<PlaceModel>> >
    get() = _places


    fun getPLaces(){
        _places.value = UiState.Loading
        repository.getPlaces { _places.value = it }
    }



}