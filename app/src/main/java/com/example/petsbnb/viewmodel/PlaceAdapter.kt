package com.example.petsbnb.viewmodel

import java.math.RoundingMode
import java.text.DecimalFormat
import kotlin.math.atan2
import kotlin.math.cos
import kotlin.math.sin
import kotlin.math.sqrt

class PlaceAdapter(
    var id: String, var barrio : String, var ciudad : String, var description: String,
    var precio: Int, var titulo: String, var ubicacion: MutableMap<String, Double>,
    var userLat  : Double, var userLng : Double) {

    val distance = getDistanceInKm()

    /*
     * calculate the distance(km) between this place and the latitude and longitude specified in parameters
     */
    private fun getDistanceInKm():Double {
        val dLat = deg2rad(ubicacion["latitud"]!! - userLat)
        val dLon = deg2rad(ubicacion["longitud"]!! - userLng)
        val a = sin(dLat/2) * sin(dLat/2) + cos(deg2rad(userLat)) * cos(deg2rad(ubicacion["latitud"]!!)) * sin(dLon/2) * sin(dLon/2)
        val c = 2 * atan2(sqrt(a), sqrt(1-a))
        return roundOffDecimal(6371 * c) // distance in Km (6371 is the earth radius)
    }

    private fun deg2rad(deg : Double) : Double {
        return deg * (Math.PI/180)
    }

    fun roundOffDecimal(number: Double): Double {
        val df = DecimalFormat("#.##")
        df.roundingMode = RoundingMode.CEILING
        return df.format(number).toDouble()
    }

}