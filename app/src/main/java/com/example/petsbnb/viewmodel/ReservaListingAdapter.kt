package com.example.petsbnb.viewmodel

import android.content.Context
import android.content.SharedPreferences
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.petsbnb.data.model.ReservaModel
import com.example.petsbnb.data.model.SitterModel
import com.example.petsbnb.databinding.ItemReservaLayoutBinding
import com.example.petsbnb.databinding.ItemSitterLayoutBinding
import com.squareup.picasso.Picasso

class ReservaListingAdapter(
    val onItemClicked: (Int, ReservaModel) -> Unit
):   RecyclerView.Adapter<ReservaListingAdapter.MyViewHolder>() {


    private var list: MutableList<ReservaModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = ItemReservaLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]
        holder.bind(item)
    }

    fun updateList(List: MutableList<ReservaModel>){
        this.list = List
        notifyDataSetChanged()
    }


    fun removeItem(position: Int){
        list.removeAt(position)
        notifyItemChanged(position)
    }


    override fun getItemCount(): Int {
        return list.size
    }




    inner class MyViewHolder(val binding: ItemReservaLayoutBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: ReservaModel){

            binding.txtInitDateReserve.setText(item.initialDate)
            binding.txtEndDateReserve.setText(item.finishDate)
            binding.txtCommentReserve.setText(item.comment)
            binding.txtCostReserve.setText(item.cost.toString())


            binding.itemLayoutSitters.setOnClickListener{ onItemClicked.invoke(adapterPosition,item) }
        }

    }





}