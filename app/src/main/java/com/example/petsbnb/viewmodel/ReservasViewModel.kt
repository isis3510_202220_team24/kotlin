package com.example.petsbnb.viewmodel;


import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.petsbnb.data.repository.ReservaRepository
import androidx.lifecycle.ViewModel
import com.example.petsbnb.data.model.ReservaModel
import com.example.petsbnb.util.UiState
import javax.inject.Inject
import dagger.hilt.android.lifecycle.HiltViewModel;

@HiltViewModel
class ReservasViewModel @Inject constructor(
    val repository: ReservaRepository
): ViewModel() {

    private val _reservas = MutableLiveData<UiState<List<ReservaModel>>>()
    val reservaModel: LiveData< UiState<List<ReservaModel>> >
        get() = _reservas


    private val _addReserva = MutableLiveData<UiState<String>>()
    val addReserva: LiveData< UiState<String>>
        get() = _addReserva


    fun getReservas(){
        _reservas.value = UiState.Loading
        repository.getReservas { _reservas.value = it }
    }



    fun addReserva(reserva:ReservaModel){
        _addReserva.value = UiState.Loading
        repository.addReserva(reserva){ _addReserva.value = it}
    }










}
