package com.example.petsbnb.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.petsbnb.data.model.SitterModel
import com.example.petsbnb.data.repository.SittersRepository
import com.example.petsbnb.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class SittersViewModel @Inject constructor(
    val repository: SittersRepository
): ViewModel(){

    private val _sitters = MutableLiveData< UiState<List<SitterModel>> >()
    val SitterModel: LiveData< UiState<List<SitterModel>> >
        get() = _sitters



    fun getSitters(){
        _sitters.value = UiState.Loading
        repository.getSitters { _sitters.value = it }
    }


}