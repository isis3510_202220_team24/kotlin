package com.example.petsbnb.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.petsbnb.data.model.ReviewModel
import com.example.petsbnb.data.repository.ReviewRepository
import com.example.petsbnb.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class ReviewViewModel @Inject constructor(
    val repository: ReviewRepository
): ViewModel() {

    private val _reviews = MutableLiveData< UiState<List<ReviewModel>> >()
    val reviewModel: LiveData<UiState<List<ReviewModel>>>
        get() = _reviews

    private val _addReview = MutableLiveData< UiState<String> >()
    val addReview: LiveData<UiState<String>>
        get() = _addReview


    private val _updateReview = MutableLiveData<UiState<String>>()
    val updateReview: LiveData<UiState<String>>
        get() = _updateReview

    private val _deleteReview = MutableLiveData<UiState<String>>()
    val deleteReview: LiveData<UiState<String>>
        get() = _deleteReview


    fun getReviews(){
        _reviews.value = UiState.Loading
        repository.getReviews { _reviews.value = it }
    }

    fun addReview(review: ReviewModel){
        _addReview.value = UiState.Loading
        repository.addReview(review){ _addReview.value = it }
    }

    fun updateReview(review: ReviewModel){
        _updateReview.value = UiState.Loading
        repository.updateReview(review){_updateReview.value = it}
    }

    fun deletePet(review: ReviewModel){
        _deleteReview.value = UiState.Loading
        repository.deleteReview(review) {_deleteReview.value = it}

    }

    fun onUploadSingleFile(fileUris: Uri, onResult: (UiState<Uri>) -> Unit){
        onResult.invoke(UiState.Loading)
        viewModelScope.launch {
            repository.uploadSingleFile(fileUris,onResult)
        }
    }
}