package com.example.petsbnb.viewmodel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.petsbnb.data.model.ReviewModel
import com.example.petsbnb.databinding.ItemReviewLayoutBinding

class ReviewListingAdapter(
    val onItemClicked: (Int,ReviewModel) -> Unit
): RecyclerView.Adapter<ReviewListingAdapter.MyViewHolder>(){

    private var list: MutableList<ReviewModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder{
        val itemView = ItemReviewLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]
        holder.bind(item)
    }

    fun updateList(list: MutableList<ReviewModel>){
        this.list = list
        notifyDataSetChanged()
    }

    fun removeItem(position: Int){
        list.removeAt(position)
        notifyItemChanged(position)
    }


    override fun getItemCount(): Int {
        return list.size
    }



    inner class MyViewHolder(val binding: ItemReviewLayoutBinding) : RecyclerView.ViewHolder(binding.root){
        fun bind(item: ReviewModel){
            binding.score.setText(item.score.toString())
            binding.sitter.setText(item.sitter.toString())
            binding.place.setText(item.place.toString())
            binding.comment.setText(item.comment.toString())
            binding.author.setText(item.username.toString())

            binding.itemLayout.setOnClickListener { onItemClicked.invoke(adapterPosition, item) }
        }
    }
}