package com.example.petsbnb.viewmodel

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.petsbnb.data.model.PlaceModel
import com.example.petsbnb.data.model.SitterModel
import com.example.petsbnb.data.repository.PlaceRepository
import com.example.petsbnb.databinding.ActivityMainBinding
import com.example.petsbnb.databinding.ItemPlaceLayoutBinding
import com.example.petsbnb.databinding.ItemSitterLayoutBinding
//import com.example.petsbnb.databinding.ItemPlaceLayoutBinding
import com.squareup.picasso.Picasso

class PlacesListingAdapter(
    val onItemClicked: (Int,PlaceModel) -> Unit
): RecyclerView.Adapter<PlacesListingAdapter.MyViewHolder>() {

    private var list: MutableList<PlaceModel> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val itemView = ItemPlaceLayoutBinding.inflate(LayoutInflater.from(parent.context),parent,false)
        return MyViewHolder(itemView)
    }


    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val item = list[position]
        holder.bind(item)
    }


    fun updateList(List: MutableList<PlaceModel>){
        this.list = List
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return list.size
    }



    inner class MyViewHolder(val binding: ItemPlaceLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PlaceModel) {
            binding.txtTituloPlace.setText(item.titulo)
            binding.txtCiudadPlace.setText(item.ciudad)
            binding.txtBarrioPlace.setText(item.barrio)
            binding.txtDescriptionPlace.setText(item.description)
            binding.txtPricePlace.setText(item.precio.toString())
        }
    }

}