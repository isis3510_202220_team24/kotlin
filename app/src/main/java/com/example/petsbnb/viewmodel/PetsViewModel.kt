package com.example.petsbnb.viewmodel

import android.net.Uri
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.petsbnb.data.model.PetModel
import com.example.petsbnb.data.repository.PetsRepository
import com.example.petsbnb.util.UiState
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

@HiltViewModel
class PetsViewModel @Inject constructor(
    val repository: PetsRepository
): ViewModel() {


    private val _pets = MutableLiveData< UiState<List<PetModel>> >()
    val petModel: LiveData< UiState<List<PetModel>> >
        get() = _pets


    private val _addPet = MutableLiveData< UiState<String> >()
    val addPet: LiveData< UiState< String > >
        get() = _addPet


    private val _updatePet = MutableLiveData<UiState<String>>()
    val updatePet: LiveData<UiState<String>>
        get() = _updatePet

    private val _deletePet = MutableLiveData<UiState<String>>()
    val deletePet: LiveData<UiState<String>>
        get() = _deletePet






    fun getPets(){
        _pets.value = UiState.Loading
        repository.getPets { _pets.value = it }
    }

    fun addPet(pet: PetModel){
        _addPet.value = UiState.Loading
        repository.addPet(pet){ _addPet.value = it }
    }



    fun updatePet(pet: PetModel){
        _updatePet.value = UiState.Loading
        repository.updatePet(pet){_updatePet.value = it}
    }


    fun deletePet(pet: PetModel){
        _deletePet.value = UiState.Loading
        repository.deletePet(pet) {_deletePet.value = it}

    }





    fun onUploadSingleFile(fileUris: Uri, onResult: (UiState<Uri>) -> Unit){
        onResult.invoke(UiState.Loading)
        viewModelScope.launch {
            repository.uploadSingleFile(fileUris,onResult)
        }
    }



}

