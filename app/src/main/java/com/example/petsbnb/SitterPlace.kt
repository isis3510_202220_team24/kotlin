package com.example.petsbnb

class SitterPlace {
    var mTitle: String?
    var mCity: String?
    var mLatitude: Double
    var mLongitude: Double
    var mDescription: String?
    var mEstablished: String?
    var mImageURL: String?

    constructor(Title:String, City:String, Latitude:Double, Longitude:Double, Description:String, Established:String, ImageURL:String){
        mTitle = Title
        mCity = City
        mLatitude = Latitude
        mLongitude = Longitude
        mDescription = Description
        mEstablished = Established
        mImageURL = ImageURL
    }
}