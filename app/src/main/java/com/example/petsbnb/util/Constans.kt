package com.example.petsbnb.util

object FireStoreTables {

    val PET = "pets"
    //val SITTER = "sitters"
    val SITTER = "sitters3"
    val RESERVA = "reserves"
    val REVIEW = "reviews"


    val PLACE = "places"

    val USERPBNB = "USERPBNB"
    val USER = "user"


}

object SharedPrefConstants {
    val LOCAL_SHARED_PREF = "local_shared_pref"
    val USER_SESSION = "user_session"
}



object FirebaseStorageConstants{
    val ROOT_DIRECTORY = "app"
    val NOTE_IMAGES = "note"
}