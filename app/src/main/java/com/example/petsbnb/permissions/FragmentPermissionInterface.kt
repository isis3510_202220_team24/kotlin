package com.example.petsbnb.permissions

interface FragmentPermissionInterface {
    fun onGranted(isGranted:Boolean)
}